webpackHotUpdate_N_E("pages/jobOrders/[jobOrderId]/newJobOffer",{

/***/ "./lib/mockaxios.ts":
/*!**************************!*\
  !*** ./lib/mockaxios.ts ***!
  \**************************/
/*! exports provided: apiInstance */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "apiInstance", function() { return apiInstance; });
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

// export interface AxiosInstance2 {
//   (config: AxiosRequestConfig): AxiosPromise;
//   (url: string, config?: AxiosRequestConfig): AxiosPromise
// }
var company = {
  name: '',
  NIP: ''
};
var orders = [{
  id: 0
}] || false;
var offers = [{
  jobOfferId: 0,
  priceNet: 1500
}];
var apiInstance = function apiInstance(url, config) {
  // this.defaults = {} as AxiosRequestConfig;
  // this.interceptors = {
  //   request: {} as AxiosInterceptorManager<AxiosRequestConfig>;
  //   response: {} as AxiosInterceptorManager<AxiosResponse<any>>;
  // };
  // this.getUri = () => {return ''};
  // this.get = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.patch = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.delete = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.post = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.put = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.request = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.head = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.options = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  return {};
};
apiInstance.defaults = {};
apiInstance.interceptors = {
  request: {},
  response: {}
};

apiInstance.getUri = function () {
  return '';
};

apiInstance.request = function (req) {
  return new Promise(function () {});
};

apiInstance.get = function (url, config) {
  return new Promise(function (res2, rej) {
    console.warn('GET', url); // debugger

    var res = function res(r) {
      console.info('GET url', url, r);
      res2({
        data: {
          data: {
            results: r
          }
        }
      });
    };

    if (url.includes('/me')) {
      return res2({
        data: {
          data: company
        }
      });
    }

    if (url.includes('/job-orders/') && url.includes('/offers')) {
      var id = parseInt(url.match(/\d+/)[0]);
      console.log('/job-orders/:id/offers', id, offers); // debugger

      return res(offers);
    }

    if (url.includes('/job-orders/')) {
      var _id = parseInt(url.match(/\d+/)[0]);

      return res2({
        data: {
          data: orders[_id]
        }
      });
    }

    if (url.includes('/job-orders')) {
      console.log('orders', orders);
      return res(orders);
    } // debugger


    if (url.includes('/job-offers')) {
      return res(offers);
    }

    if (url.includes('/categories/prod')) {
      return res2({
        data: {
          data: [{
            label: 'Fotowoltaika',
            name: 'Fotowoltaika',
            value: 'Fotowoltaika'
          }, {
            label: 'Pompy ciepła',
            name: 'Pompy ciepła',
            value: 'Pompy ciepła'
          }]
        }
      });
    }

    if (url.includes('/categories/job')) {
      // debugger
      return res2({
        data: {
          data: [{
            label: 'Montaż',
            name: 'Montaż',
            value: 'Montaż'
          }, {
            label: 'Projekt',
            name: 'Projekt',
            value: 'Projekt'
          }]
        }
      });
    }

    rej('err');
  });
};

apiInstance.patch = function (url, data, config) {
  return new Promise(function (res) {
    company = _objectSpread(_objectSpread({}, company), data); // debugger

    console.info('saved companies/me', company);
    return res(company);
  });
};

apiInstance["delete"] = function (url, config) {
  return new Promise(function () {});
};

apiInstance.post = function (url, data, config) {
  // return new Promise(()=>{})
  return new Promise(function (res2, rej) {
    console.log('POST', url, data);
    debugger;

    var res = function res(r) {
      return res2({
        data: {
          data: {
            results: r
          }
        }
      });
    };

    if (url.includes('/offers')) {
      var jobId = parseInt(url.match(/\d+/)[0]);
      data = _objectSpread(_objectSpread({
        id: offers.length || 0
      }, data), {}, {
        jobOrderId: jobId
      }); // offers.push(data)
      // return res(offers.slice(-1)[0])

      offers.splice(0, 0, data);
      return res(offers.slice(0)[0]);
    }

    if (url.includes('/job-orders')) {
      // debugger
      data = _objectSpread(_objectSpread({
        id: orders.length || 0,
        creator: {
          company: company
        }
      }, data), {}, {
        acceptingOffersUntil: new Date(data.acceptingOffersUntil).toISOString(),
        executionUntil: new Date(data.executionUntil).toISOString()
      });
      console.log('POST job-order', data); // orders.push(data)
      // return res(orders.slice(-1)[0])

      orders.splice(0, 0, data);
      return res(orders[0]);
    }

    rej('err');
  });
};

apiInstance.put = function (url, config) {
  return new Promise(function () {});
};

apiInstance.head = function (url, config) {
  return new Promise(function () {});
};

apiInstance.options = function (url, config) {
  return new Promise(function () {});
};
/*


export class API2 implements AxiosInstance {
  // constructor(url: string | AxiosRequestConfig, config?: AxiosRequestConfig) {
  // }
  constructor(config: AxiosRequestConfig): AxiosPromise<any> {
  }
  defaults: {};
  // interceptors: {request: {use(){return 0}, eject(){}}, response: {use(){return 0}, eject(){}}};
  interceptors: {
    request: AxiosInterceptorManager<AxiosRequestConfig>;
    response: AxiosInterceptorManager<AxiosResponse<any>>;
  };
  // getUri(){return ''};
  // get(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // patch(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // delete(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // post(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // put(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // request(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // head(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // options(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  getUri(config?: AxiosRequestConfig): string {return ''};
  request<T = any, R = AxiosResponse<T>> (config: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  get<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  delete<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  head<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  options<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  post<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  put<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  patch<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
}

// export const apiInstance: AxiosInstance = function (url: string | AxiosRequestConfig, config?: AxiosRequestConfig): AxiosPromise {
// }

*/

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "../../node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vbGliL21vY2theGlvcy50cyJdLCJuYW1lcyI6WyJjb21wYW55IiwibmFtZSIsIk5JUCIsIm9yZGVycyIsImlkIiwib2ZmZXJzIiwiam9iT2ZmZXJJZCIsInByaWNlTmV0IiwiYXBpSW5zdGFuY2UiLCJ1cmwiLCJjb25maWciLCJkZWZhdWx0cyIsImludGVyY2VwdG9ycyIsInJlcXVlc3QiLCJyZXNwb25zZSIsImdldFVyaSIsInJlcSIsIlByb21pc2UiLCJnZXQiLCJyZXMyIiwicmVqIiwiY29uc29sZSIsIndhcm4iLCJyZXMiLCJyIiwiaW5mbyIsImRhdGEiLCJyZXN1bHRzIiwiaW5jbHVkZXMiLCJwYXJzZUludCIsIm1hdGNoIiwibG9nIiwibGFiZWwiLCJ2YWx1ZSIsInBhdGNoIiwicG9zdCIsImpvYklkIiwibGVuZ3RoIiwiam9iT3JkZXJJZCIsInNwbGljZSIsInNsaWNlIiwiY3JlYXRvciIsImFjY2VwdGluZ09mZmVyc1VudGlsIiwiRGF0ZSIsInRvSVNPU3RyaW5nIiwiZXhlY3V0aW9uVW50aWwiLCJwdXQiLCJoZWFkIiwib3B0aW9ucyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsSUFBSUEsT0FBTyxHQUFHO0FBQ1pDLE1BQUksRUFBRSxFQURNO0FBRVpDLEtBQUcsRUFBRTtBQUZPLENBQWQ7QUFJQSxJQUFJQyxNQUFNLEdBQUcsQ0FBQztBQUFDQyxJQUFFLEVBQUU7QUFBTCxDQUFELEtBQWEsS0FBMUI7QUFLQSxJQUFJQyxNQUFNLEdBQUcsQ0FBQztBQUNaQyxZQUFVLEVBQUUsQ0FEQTtBQUVaQyxVQUFRLEVBQUU7QUFGRSxDQUFELENBQWI7QUFLTyxJQUFNQyxXQUEwQixHQUFHLFNBQTdCQSxXQUE2QixDQUFVQyxHQUFWLEVBQTRDQyxNQUE1QyxFQUF5RTtBQUNqSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsU0FBTyxFQUFQO0FBQ0QsQ0FsQk07QUFtQlBGLFdBQVcsQ0FBQ0csUUFBWixHQUF1QixFQUF2QjtBQUNBSCxXQUFXLENBQUNJLFlBQVosR0FBMkI7QUFDekJDLFNBQU8sRUFBRSxFQURnQjtBQUV6QkMsVUFBUSxFQUFFO0FBRmUsQ0FBM0I7O0FBSUFOLFdBQVcsQ0FBQ08sTUFBWixHQUFxQixZQUFNO0FBQUMsU0FBTyxFQUFQO0FBQVUsQ0FBdEM7O0FBQ0FQLFdBQVcsQ0FBQ0ssT0FBWixHQUFzQixVQUFDRyxHQUFELEVBQTZCO0FBQUMsU0FBTyxJQUFJQyxPQUFKLENBQVksWUFBSSxDQUFFLENBQWxCLENBQVA7QUFBMkIsQ0FBL0U7O0FBQ0FULFdBQVcsQ0FBQ1UsR0FBWixHQUFrQixVQUFDVCxHQUFELEVBQWNDLE1BQWQsRUFBOEM7QUFDOUQsU0FBTyxJQUFJTyxPQUFKLENBQVksVUFBQ0UsSUFBRCxFQUFPQyxHQUFQLEVBQWU7QUFDaENDLFdBQU8sQ0FBQ0MsSUFBUixDQUFhLEtBQWIsRUFBb0JiLEdBQXBCLEVBRGdDLENBRWhDOztBQUNBLFFBQU1jLEdBQUcsR0FBRyxTQUFOQSxHQUFNLENBQUNDLENBQUQsRUFBTztBQUNqQkgsYUFBTyxDQUFDSSxJQUFSLENBQWEsU0FBYixFQUF3QmhCLEdBQXhCLEVBQTZCZSxDQUE3QjtBQUNBTCxVQUFJLENBQUM7QUFBQ08sWUFBSSxFQUFFO0FBQUNBLGNBQUksRUFBRTtBQUFDQyxtQkFBTyxFQUFFSDtBQUFWO0FBQVA7QUFBUCxPQUFELENBQUo7QUFDRCxLQUhEOztBQUlBLFFBQUlmLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxLQUFiLENBQUosRUFBeUI7QUFDdkIsYUFBT1QsSUFBSSxDQUFDO0FBQUNPLFlBQUksRUFBRTtBQUFDQSxjQUFJLEVBQUUxQjtBQUFQO0FBQVAsT0FBRCxDQUFYO0FBQ0Q7O0FBQ0QsUUFBSVMsR0FBRyxDQUFDbUIsUUFBSixDQUFhLGNBQWIsS0FBZ0NuQixHQUFHLENBQUNtQixRQUFKLENBQWEsU0FBYixDQUFwQyxFQUE2RDtBQUMzRCxVQUFNeEIsRUFBRSxHQUFHeUIsUUFBUSxDQUFDcEIsR0FBRyxDQUFDcUIsS0FBSixDQUFVLEtBQVYsRUFBaUIsQ0FBakIsQ0FBRCxDQUFuQjtBQUNBVCxhQUFPLENBQUNVLEdBQVIsQ0FBWSx3QkFBWixFQUFzQzNCLEVBQXRDLEVBQTBDQyxNQUExQyxFQUYyRCxDQUczRDs7QUFDQSxhQUFPa0IsR0FBRyxDQUFDbEIsTUFBRCxDQUFWO0FBQ0Q7O0FBQ0QsUUFBSUksR0FBRyxDQUFDbUIsUUFBSixDQUFhLGNBQWIsQ0FBSixFQUFrQztBQUNoQyxVQUFNeEIsR0FBRSxHQUFHeUIsUUFBUSxDQUFDcEIsR0FBRyxDQUFDcUIsS0FBSixDQUFVLEtBQVYsRUFBaUIsQ0FBakIsQ0FBRCxDQUFuQjs7QUFDQSxhQUFPWCxJQUFJLENBQUM7QUFBQ08sWUFBSSxFQUFFO0FBQUNBLGNBQUksRUFBRXZCLE1BQU0sQ0FBQ0MsR0FBRDtBQUFiO0FBQVAsT0FBRCxDQUFYO0FBQ0Q7O0FBQ0QsUUFBSUssR0FBRyxDQUFDbUIsUUFBSixDQUFhLGFBQWIsQ0FBSixFQUFpQztBQUMvQlAsYUFBTyxDQUFDVSxHQUFSLENBQVksUUFBWixFQUFzQjVCLE1BQXRCO0FBQ0EsYUFBT29CLEdBQUcsQ0FBQ3BCLE1BQUQsQ0FBVjtBQUNELEtBdkIrQixDQXdCaEM7OztBQUNBLFFBQUlNLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxhQUFiLENBQUosRUFBaUM7QUFDL0IsYUFBT0wsR0FBRyxDQUFDbEIsTUFBRCxDQUFWO0FBQ0Q7O0FBQ0QsUUFBSUksR0FBRyxDQUFDbUIsUUFBSixDQUFhLGtCQUFiLENBQUosRUFBc0M7QUFDcEMsYUFBT1QsSUFBSSxDQUFDO0FBQUNPLFlBQUksRUFBRTtBQUFDQSxjQUFJLEVBQ3RCLENBQUM7QUFBQ00saUJBQUssRUFBRSxjQUFSO0FBQXdCL0IsZ0JBQUksRUFBRSxjQUE5QjtBQUE4Q2dDLGlCQUFLLEVBQUU7QUFBckQsV0FBRCxFQUFzRTtBQUFDRCxpQkFBSyxFQUFFLGNBQVI7QUFBd0IvQixnQkFBSSxFQUFFLGNBQTlCO0FBQThDZ0MsaUJBQUssRUFBRTtBQUFyRCxXQUF0RTtBQURpQjtBQUFQLE9BQUQsQ0FBWDtBQUdEOztBQUNELFFBQUl4QixHQUFHLENBQUNtQixRQUFKLENBQWEsaUJBQWIsQ0FBSixFQUFxQztBQUNuQztBQUNBLGFBQU9ULElBQUksQ0FBQztBQUFDTyxZQUFJLEVBQUU7QUFBQ0EsY0FBSSxFQUNyQixDQUFDO0FBQUNNLGlCQUFLLEVBQUUsUUFBUjtBQUFrQi9CLGdCQUFJLEVBQUUsUUFBeEI7QUFBa0NnQyxpQkFBSyxFQUFFO0FBQXpDLFdBQUQsRUFBb0Q7QUFBQ0QsaUJBQUssRUFBRSxTQUFSO0FBQW1CL0IsZ0JBQUksRUFBRSxTQUF6QjtBQUFvQ2dDLGlCQUFLLEVBQUU7QUFBM0MsV0FBcEQ7QUFEZ0I7QUFBUCxPQUFELENBQVg7QUFHRDs7QUFDRGIsT0FBRyxDQUFDLEtBQUQsQ0FBSDtBQUNELEdBeENNLENBQVA7QUF5Q0QsQ0ExQ0Q7O0FBMkNBWixXQUFXLENBQUMwQixLQUFaLEdBQW9CLFVBQUN6QixHQUFELEVBQWNpQixJQUFkLEVBQXlCaEIsTUFBekIsRUFBeUQ7QUFBQyxTQUFPLElBQUlPLE9BQUosQ0FBaUIsVUFBQ00sR0FBRCxFQUFPO0FBQzNHdkIsV0FBTyxtQ0FBT0EsT0FBUCxHQUFtQjBCLElBQW5CLENBQVAsQ0FEMkcsQ0FFM0c7O0FBQ0FMLFdBQU8sQ0FBQ0ksSUFBUixDQUFhLG9CQUFiLEVBQW1DekIsT0FBbkM7QUFDQSxXQUFPdUIsR0FBRyxDQUFDdkIsT0FBRCxDQUFWO0FBQ0QsR0FMb0YsQ0FBUDtBQUszRSxDQUxIOztBQU1BUSxXQUFXLFVBQVgsR0FBcUIsVUFBQ0MsR0FBRCxFQUFjQyxNQUFkLEVBQThDO0FBQUMsU0FBTyxJQUFJTyxPQUFKLENBQVksWUFBSSxDQUFFLENBQWxCLENBQVA7QUFBMkIsQ0FBL0Y7O0FBQ0FULFdBQVcsQ0FBQzJCLElBQVosR0FBbUIsVUFBQzFCLEdBQUQsRUFBY2lCLElBQWQsRUFBeUJoQixNQUF6QixFQUF5RDtBQUMxRTtBQUNBLFNBQU8sSUFBSU8sT0FBSixDQUFZLFVBQUNFLElBQUQsRUFBT0MsR0FBUCxFQUFlO0FBQ2hDQyxXQUFPLENBQUNVLEdBQVIsQ0FBWSxNQUFaLEVBQW9CdEIsR0FBcEIsRUFBeUJpQixJQUF6QjtBQUNBOztBQUNBLFFBQU1ILEdBQUcsR0FBRyxTQUFOQSxHQUFNLENBQUNDLENBQUQ7QUFBQSxhQUFPTCxJQUFJLENBQUM7QUFBQ08sWUFBSSxFQUFFO0FBQUNBLGNBQUksRUFBRTtBQUFDQyxtQkFBTyxFQUFFSDtBQUFWO0FBQVA7QUFBUCxPQUFELENBQVg7QUFBQSxLQUFaOztBQUNBLFFBQUlmLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxTQUFiLENBQUosRUFBNkI7QUFDM0IsVUFBTVEsS0FBSyxHQUFHUCxRQUFRLENBQUNwQixHQUFHLENBQUNxQixLQUFKLENBQVUsS0FBVixFQUFpQixDQUFqQixDQUFELENBQXRCO0FBQ0FKLFVBQUk7QUFDRnRCLFVBQUUsRUFBR0MsTUFBTSxDQUFDZ0MsTUFBUCxJQUFpQjtBQURwQixTQUVDWCxJQUZEO0FBR0ZZLGtCQUFVLEVBQUVGO0FBSFYsUUFBSixDQUYyQixDQU8zQjtBQUNBOztBQUNBL0IsWUFBTSxDQUFDa0MsTUFBUCxDQUFjLENBQWQsRUFBaUIsQ0FBakIsRUFBb0JiLElBQXBCO0FBQ0EsYUFBT0gsR0FBRyxDQUFDbEIsTUFBTSxDQUFDbUMsS0FBUCxDQUFhLENBQWIsRUFBZ0IsQ0FBaEIsQ0FBRCxDQUFWO0FBQ0Q7O0FBQ0QsUUFBSS9CLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxhQUFiLENBQUosRUFBaUM7QUFDL0I7QUFDQUYsVUFBSTtBQUNGdEIsVUFBRSxFQUFHRCxNQUFNLENBQUNrQyxNQUFQLElBQWlCLENBRHBCO0FBRUZJLGVBQU8sRUFBRTtBQUNQekMsaUJBQU8sRUFBUEE7QUFETztBQUZQLFNBS0MwQixJQUxEO0FBTUZnQiw0QkFBb0IsRUFBRSxJQUFJQyxJQUFKLENBQVNqQixJQUFJLENBQUNnQixvQkFBZCxFQUFvQ0UsV0FBcEMsRUFOcEI7QUFPRkMsc0JBQWMsRUFBRSxJQUFJRixJQUFKLENBQVNqQixJQUFJLENBQUNtQixjQUFkLEVBQThCRCxXQUE5QjtBQVBkLFFBQUo7QUFTQXZCLGFBQU8sQ0FBQ1UsR0FBUixDQUFZLGdCQUFaLEVBQThCTCxJQUE5QixFQVgrQixDQVkvQjtBQUNBOztBQUNBdkIsWUFBTSxDQUFDb0MsTUFBUCxDQUFjLENBQWQsRUFBaUIsQ0FBakIsRUFBb0JiLElBQXBCO0FBQ0EsYUFBT0gsR0FBRyxDQUFDcEIsTUFBTSxDQUFDLENBQUQsQ0FBUCxDQUFWO0FBQ0Q7O0FBQ0RpQixPQUFHLENBQUMsS0FBRCxDQUFIO0FBQ0QsR0FsQ00sQ0FBUDtBQW1DRCxDQXJDRDs7QUFzQ0FaLFdBQVcsQ0FBQ3NDLEdBQVosR0FBa0IsVUFBQ3JDLEdBQUQsRUFBY0MsTUFBZCxFQUE4QztBQUFDLFNBQU8sSUFBSU8sT0FBSixDQUFZLFlBQUksQ0FBRSxDQUFsQixDQUFQO0FBQTJCLENBQTVGOztBQUNBVCxXQUFXLENBQUN1QyxJQUFaLEdBQW1CLFVBQUN0QyxHQUFELEVBQWNDLE1BQWQsRUFBOEM7QUFBQyxTQUFPLElBQUlPLE9BQUosQ0FBWSxZQUFJLENBQUUsQ0FBbEIsQ0FBUDtBQUEyQixDQUE3Rjs7QUFDQVQsV0FBVyxDQUFDd0MsT0FBWixHQUFzQixVQUFDdkMsR0FBRCxFQUFjQyxNQUFkLEVBQThDO0FBQUMsU0FBTyxJQUFJTyxPQUFKLENBQVksWUFBSSxDQUFFLENBQWxCLENBQVA7QUFBMkIsQ0FBaEc7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvam9iT3JkZXJzL1tqb2JPcmRlcklkXS9uZXdKb2JPZmZlci43YjM0ZTUzZTZhN2U3OTZhNzI5Ni5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXhpb3NJbnN0YW5jZSwgQXhpb3NJbnRlcmNlcHRvck1hbmFnZXIsIEF4aW9zUHJvbWlzZSwgQXhpb3NSZXF1ZXN0Q29uZmlnLCBBeGlvc1Jlc3BvbnNlIH0gZnJvbSBcImF4aW9zXCI7XHJcblxyXG4vLyBleHBvcnQgaW50ZXJmYWNlIEF4aW9zSW5zdGFuY2UyIHtcclxuLy8gICAoY29uZmlnOiBBeGlvc1JlcXVlc3RDb25maWcpOiBBeGlvc1Byb21pc2U7XHJcbi8vICAgKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpOiBBeGlvc1Byb21pc2VcclxuLy8gfVxyXG5cclxubGV0IGNvbXBhbnkgPSB7XHJcbiAgbmFtZTogJycsXHJcbiAgTklQOiAnJyxcclxufVxyXG5sZXQgb3JkZXJzID0gW3tpZDogMH1dIHx8IFt7XHJcbiAgY3JlYXRvcjoge1xyXG4gICAgY29tcGFueTogey4uLmNvbXBhbnl9LFxyXG4gIH0sXHJcbn1dXHJcbmxldCBvZmZlcnMgPSBbe1xyXG4gIGpvYk9mZmVySWQ6IDAsXHJcbiAgcHJpY2VOZXQ6IDE1MDAsXHJcbn1dXHJcblxyXG5leHBvcnQgY29uc3QgYXBpSW5zdGFuY2U6IEF4aW9zSW5zdGFuY2UgPSBmdW5jdGlvbiAodXJsOiBzdHJpbmcgfCBBeGlvc1JlcXVlc3RDb25maWcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykge1xyXG4gIC8vIHRoaXMuZGVmYXVsdHMgPSB7fSBhcyBBeGlvc1JlcXVlc3RDb25maWc7XHJcbiAgLy8gdGhpcy5pbnRlcmNlcHRvcnMgPSB7XHJcbiAgLy8gICByZXF1ZXN0OiB7fSBhcyBBeGlvc0ludGVyY2VwdG9yTWFuYWdlcjxBeGlvc1JlcXVlc3RDb25maWc+O1xyXG4gIC8vICAgcmVzcG9uc2U6IHt9IGFzIEF4aW9zSW50ZXJjZXB0b3JNYW5hZ2VyPEF4aW9zUmVzcG9uc2U8YW55Pj47XHJcbiAgLy8gfTtcclxuICBcclxuICAvLyB0aGlzLmdldFVyaSA9ICgpID0+IHtyZXR1cm4gJyd9O1xyXG4gIC8vIHRoaXMuZ2V0ID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gdGhpcy5wYXRjaCA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHRoaXMuZGVsZXRlID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gdGhpcy5wb3N0ID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gdGhpcy5wdXQgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyB0aGlzLnJlcXVlc3QgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyB0aGlzLmhlYWQgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyB0aGlzLm9wdGlvbnMgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuXHJcbiAgcmV0dXJuIHt9IGFzIEF4aW9zUHJvbWlzZTxhbnk+O1xyXG59XHJcbmFwaUluc3RhbmNlLmRlZmF1bHRzID0ge30gYXMgQXhpb3NSZXF1ZXN0Q29uZmlnXHJcbmFwaUluc3RhbmNlLmludGVyY2VwdG9ycyA9IHtcclxuICByZXF1ZXN0OiB7fSBhcyBBeGlvc0ludGVyY2VwdG9yTWFuYWdlcjxBeGlvc1JlcXVlc3RDb25maWc+LFxyXG4gIHJlc3BvbnNlOiB7fSBhcyBBeGlvc0ludGVyY2VwdG9yTWFuYWdlcjxBeGlvc1Jlc3BvbnNlPGFueT4+XHJcbn1cclxuYXBpSW5zdGFuY2UuZ2V0VXJpID0gKCkgPT4ge3JldHVybiAnJ307XHJcbmFwaUluc3RhbmNlLnJlcXVlc3QgPSAocmVxOiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbmFwaUluc3RhbmNlLmdldCA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7XHJcbiAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXMyLCByZWopID0+IHtcclxuICAgIGNvbnNvbGUud2FybignR0VUJywgdXJsKVxyXG4gICAgLy8gZGVidWdnZXJcclxuICAgIGNvbnN0IHJlcyA9IChyKSA9PiB7XHJcbiAgICAgIGNvbnNvbGUuaW5mbygnR0VUIHVybCcsIHVybCwgcilcclxuICAgICAgcmVzMih7ZGF0YToge2RhdGE6IHtyZXN1bHRzOiByfX19KVxyXG4gICAgfVxyXG4gICAgaWYgKHVybC5pbmNsdWRlcygnL21lJykpIHtcclxuICAgICAgcmV0dXJuIHJlczIoe2RhdGE6IHtkYXRhOiBjb21wYW55fX0pXHJcbiAgICB9XHJcbiAgICBpZiAodXJsLmluY2x1ZGVzKCcvam9iLW9yZGVycy8nKSAmJiB1cmwuaW5jbHVkZXMoJy9vZmZlcnMnKSkge1xyXG4gICAgICBjb25zdCBpZCA9IHBhcnNlSW50KHVybC5tYXRjaCgvXFxkKy8pWzBdKVxyXG4gICAgICBjb25zb2xlLmxvZygnL2pvYi1vcmRlcnMvOmlkL29mZmVycycsIGlkLCBvZmZlcnMpXHJcbiAgICAgIC8vIGRlYnVnZ2VyXHJcbiAgICAgIHJldHVybiByZXMob2ZmZXJzKVxyXG4gICAgfVxyXG4gICAgaWYgKHVybC5pbmNsdWRlcygnL2pvYi1vcmRlcnMvJykpIHtcclxuICAgICAgY29uc3QgaWQgPSBwYXJzZUludCh1cmwubWF0Y2goL1xcZCsvKVswXSlcclxuICAgICAgcmV0dXJuIHJlczIoe2RhdGE6IHtkYXRhOiBvcmRlcnNbaWRdfX0pXHJcbiAgICB9XHJcbiAgICBpZiAodXJsLmluY2x1ZGVzKCcvam9iLW9yZGVycycpKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKCdvcmRlcnMnLCBvcmRlcnMpXHJcbiAgICAgIHJldHVybiByZXMob3JkZXJzKVxyXG4gICAgfVxyXG4gICAgLy8gZGVidWdnZXJcclxuICAgIGlmICh1cmwuaW5jbHVkZXMoJy9qb2Itb2ZmZXJzJykpIHtcclxuICAgICAgcmV0dXJuIHJlcyhvZmZlcnMpXHJcbiAgICB9XHJcbiAgICBpZiAodXJsLmluY2x1ZGVzKCcvY2F0ZWdvcmllcy9wcm9kJykpIHtcclxuICAgICAgcmV0dXJuIHJlczIoe2RhdGE6IHtkYXRhOiBcclxuICAgICAgICBbe2xhYmVsOiAnRm90b3dvbHRhaWthJywgbmFtZTogJ0ZvdG93b2x0YWlrYScsIHZhbHVlOiAnRm90b3dvbHRhaWthJ30se2xhYmVsOiAnUG9tcHkgY2llcMWCYScsIG5hbWU6ICdQb21weSBjaWVwxYJhJywgdmFsdWU6ICdQb21weSBjaWVwxYJhJ31dXHJcbiAgICAgIH19KVxyXG4gICAgfVxyXG4gICAgaWYgKHVybC5pbmNsdWRlcygnL2NhdGVnb3JpZXMvam9iJykpIHtcclxuICAgICAgLy8gZGVidWdnZXJcclxuICAgICAgcmV0dXJuIHJlczIoe2RhdGE6IHtkYXRhOiBcclxuICAgICAgICAoW3tsYWJlbDogJ01vbnRhxbwnLCBuYW1lOiAnTW9udGHFvCcsIHZhbHVlOiAnTW9udGHFvCd9LHtsYWJlbDogJ1Byb2pla3QnLCBuYW1lOiAnUHJvamVrdCcsIHZhbHVlOiAnUHJvamVrdCd9XSlcclxuICAgICAgfX0pXHJcbiAgICB9XHJcbiAgICByZWooJ2VycicpXHJcbiAgfSkgYXMgdW5rbm93biBhcyBQcm9taXNlPGFueT5cclxufTtcclxuYXBpSW5zdGFuY2UucGF0Y2ggPSAodXJsOiBzdHJpbmcsIGRhdGE6IGFueSwgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlPGFueT4oKHJlcyk9PntcclxuICBjb21wYW55ID0gey4uLmNvbXBhbnksIC4uLmRhdGF9XHJcbiAgLy8gZGVidWdnZXJcclxuICBjb25zb2xlLmluZm8oJ3NhdmVkIGNvbXBhbmllcy9tZScsIGNvbXBhbnkpXHJcbiAgcmV0dXJuIHJlcyhjb21wYW55KVxyXG59KX07XHJcbmFwaUluc3RhbmNlLmRlbGV0ZSA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG5hcGlJbnN0YW5jZS5wb3N0ID0gKHVybDogc3RyaW5nLCBkYXRhOiBhbnksIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge1xyXG4gIC8vIHJldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pXHJcbiAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXMyLCByZWopID0+IHtcclxuICAgIGNvbnNvbGUubG9nKCdQT1NUJywgdXJsLCBkYXRhKVxyXG4gICAgZGVidWdnZXJcclxuICAgIGNvbnN0IHJlcyA9IChyKSA9PiByZXMyKHtkYXRhOiB7ZGF0YToge3Jlc3VsdHM6IHJ9fX0pXHJcbiAgICBpZiAodXJsLmluY2x1ZGVzKCcvb2ZmZXJzJykpIHtcclxuICAgICAgY29uc3Qgam9iSWQgPSBwYXJzZUludCh1cmwubWF0Y2goL1xcZCsvKVswXSlcclxuICAgICAgZGF0YSA9IHtcclxuICAgICAgICBpZDogKG9mZmVycy5sZW5ndGggfHwgMCksXHJcbiAgICAgICAgLi4uZGF0YSxcclxuICAgICAgICBqb2JPcmRlcklkOiBqb2JJZCxcclxuICAgICAgfVxyXG4gICAgICAvLyBvZmZlcnMucHVzaChkYXRhKVxyXG4gICAgICAvLyByZXR1cm4gcmVzKG9mZmVycy5zbGljZSgtMSlbMF0pXHJcbiAgICAgIG9mZmVycy5zcGxpY2UoMCwgMCwgZGF0YSlcclxuICAgICAgcmV0dXJuIHJlcyhvZmZlcnMuc2xpY2UoMClbMF0pXHJcbiAgICB9XHJcbiAgICBpZiAodXJsLmluY2x1ZGVzKCcvam9iLW9yZGVycycpKSB7XHJcbiAgICAgIC8vIGRlYnVnZ2VyXHJcbiAgICAgIGRhdGEgPSB7XHJcbiAgICAgICAgaWQ6IChvcmRlcnMubGVuZ3RoIHx8IDApLFxyXG4gICAgICAgIGNyZWF0b3I6IHtcclxuICAgICAgICAgIGNvbXBhbnksXHJcbiAgICAgICAgfSxcclxuICAgICAgICAuLi5kYXRhLFxyXG4gICAgICAgIGFjY2VwdGluZ09mZmVyc1VudGlsOiBuZXcgRGF0ZShkYXRhLmFjY2VwdGluZ09mZmVyc1VudGlsKS50b0lTT1N0cmluZygpLFxyXG4gICAgICAgIGV4ZWN1dGlvblVudGlsOiBuZXcgRGF0ZShkYXRhLmV4ZWN1dGlvblVudGlsKS50b0lTT1N0cmluZygpLFxyXG4gICAgICB9XHJcbiAgICAgIGNvbnNvbGUubG9nKCdQT1NUIGpvYi1vcmRlcicsIGRhdGEpXHJcbiAgICAgIC8vIG9yZGVycy5wdXNoKGRhdGEpXHJcbiAgICAgIC8vIHJldHVybiByZXMob3JkZXJzLnNsaWNlKC0xKVswXSlcclxuICAgICAgb3JkZXJzLnNwbGljZSgwLCAwLCBkYXRhKVxyXG4gICAgICByZXR1cm4gcmVzKG9yZGVyc1swXSlcclxuICAgIH1cclxuICAgIHJlaignZXJyJylcclxuICB9KSBhcyB1bmtub3duIGFzIFByb21pc2U8YW55PlxyXG59O1xyXG5hcGlJbnN0YW5jZS5wdXQgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuYXBpSW5zdGFuY2UuaGVhZCA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG5hcGlJbnN0YW5jZS5vcHRpb25zID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcblxyXG4vKlxyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBBUEkyIGltcGxlbWVudHMgQXhpb3NJbnN0YW5jZSB7XHJcbiAgLy8gY29uc3RydWN0b3IodXJsOiBzdHJpbmcgfCBBeGlvc1JlcXVlc3RDb25maWcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykge1xyXG4gIC8vIH1cclxuICBjb25zdHJ1Y3Rvcihjb25maWc6IEF4aW9zUmVxdWVzdENvbmZpZyk6IEF4aW9zUHJvbWlzZTxhbnk+IHtcclxuICB9XHJcbiAgZGVmYXVsdHM6IHt9O1xyXG4gIC8vIGludGVyY2VwdG9yczoge3JlcXVlc3Q6IHt1c2UoKXtyZXR1cm4gMH0sIGVqZWN0KCl7fX0sIHJlc3BvbnNlOiB7dXNlKCl7cmV0dXJuIDB9LCBlamVjdCgpe319fTtcclxuICBpbnRlcmNlcHRvcnM6IHtcclxuICAgIHJlcXVlc3Q6IEF4aW9zSW50ZXJjZXB0b3JNYW5hZ2VyPEF4aW9zUmVxdWVzdENvbmZpZz47XHJcbiAgICByZXNwb25zZTogQXhpb3NJbnRlcmNlcHRvck1hbmFnZXI8QXhpb3NSZXNwb25zZTxhbnk+PjtcclxuICB9O1xyXG4gIC8vIGdldFVyaSgpe3JldHVybiAnJ307XHJcbiAgLy8gZ2V0KHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpIHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gcGF0Y2godXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyBkZWxldGUodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyBwb3N0KHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpIHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gcHV0KHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpIHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gcmVxdWVzdCh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIGhlYWQodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyBvcHRpb25zKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpIHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgZ2V0VXJpKGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZyk6IHN0cmluZyB7cmV0dXJuICcnfTtcclxuICByZXF1ZXN0PFQgPSBhbnksIFIgPSBBeGlvc1Jlc3BvbnNlPFQ+PiAoY29uZmlnOiBBeGlvc1JlcXVlc3RDb25maWcpOiBQcm9taXNlPFI+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pXHJcbiAgfTtcclxuICBnZXQ8VCA9IGFueSwgUiA9IEF4aW9zUmVzcG9uc2U8VD4+KHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpOiBQcm9taXNlPFI+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pXHJcbiAgfTtcclxuICBkZWxldGU8VCA9IGFueSwgUiA9IEF4aW9zUmVzcG9uc2U8VD4+KHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpOiBQcm9taXNlPFI+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pXHJcbiAgfTtcclxuICBoZWFkPFQgPSBhbnksIFIgPSBBeGlvc1Jlc3BvbnNlPFQ+Pih1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogUHJvbWlzZTxSPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KVxyXG4gIH07XHJcbiAgb3B0aW9uczxUID0gYW55LCBSID0gQXhpb3NSZXNwb25zZTxUPj4odXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZyk6IFByb21pc2U8Uj4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSlcclxuICB9O1xyXG4gIHBvc3Q8VCA9IGFueSwgUiA9IEF4aW9zUmVzcG9uc2U8VD4+KHVybDogc3RyaW5nLCBkYXRhPzogYW55LCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpOiBQcm9taXNlPFI+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pXHJcbiAgfTtcclxuICBwdXQ8VCA9IGFueSwgUiA9IEF4aW9zUmVzcG9uc2U8VD4+KHVybDogc3RyaW5nLCBkYXRhPzogYW55LCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpOiBQcm9taXNlPFI+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pXHJcbiAgfTtcclxuICBwYXRjaDxUID0gYW55LCBSID0gQXhpb3NSZXNwb25zZTxUPj4odXJsOiBzdHJpbmcsIGRhdGE/OiBhbnksIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZyk6IFByb21pc2U8Uj4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSlcclxuICB9O1xyXG59XHJcblxyXG4vLyBleHBvcnQgY29uc3QgYXBpSW5zdGFuY2U6IEF4aW9zSW5zdGFuY2UgPSBmdW5jdGlvbiAodXJsOiBzdHJpbmcgfCBBeGlvc1JlcXVlc3RDb25maWcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZyk6IEF4aW9zUHJvbWlzZSB7XHJcbi8vIH1cclxuXHJcbiovIl0sInNvdXJjZVJvb3QiOiIifQ==