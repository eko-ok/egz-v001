webpackHotUpdate_N_E("pages/job/[jobId]/offers",{

/***/ "./pages/job/[jobId]/offers.tsx":
/*!**************************************!*\
  !*** ./pages/job/[jobId]/offers.tsx ***!
  \**************************************/
/*! exports provided: useJobOffers, JobOrderOffers, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useJobOffers", function() { return useJobOffers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobOrderOffers", function() { return JobOrderOffers; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "../../node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "../../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "../../node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-query */ "../../node_modules/react-query/es/index.js");
/* harmony import */ var _wayfindinghq_ecopros_kit__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wayfindinghq/ecopros-kit */ "../../node_modules/@wayfindinghq/ecopros-kit/index.js");
/* harmony import */ var _wayfindinghq_ecopros_kit_dist_components_organism_OrderListRows_OrderListRows__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wayfindinghq/ecopros-kit/dist/components/organism/OrderListRows/OrderListRows */ "../../node_modules/@wayfindinghq/ecopros-kit/dist/components/organism/OrderListRows/OrderListRows.jsx");
/* harmony import */ var hooks_useGetJobs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! hooks/useGetJobs */ "./hooks/useGetJobs.ts");
/* harmony import */ var lib_api__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lib/api */ "./lib/api.ts");
/* harmony import */ var _components_common_job_order_modal_template__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @components/common/job-order-modal/template */ "./components/common/job-order-modal/template.tsx");




var _jsxFileName = "C:\\Users\\azochniak\\Desktop\\ecop\\trading-platform\\packages\\frontend\\pages\\job\\[jobId]\\offers.tsx",
    _s = $RefreshSig$(),
    _this = undefined,
    _s2 = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }









var useJobOffers = function useJobOffers(jobId) {
  _s();

  return Object(react_query__WEBPACK_IMPORTED_MODULE_4__["useQuery"])(['job', jobId, 'offers'], function () {
    return Object(lib_api__WEBPACK_IMPORTED_MODULE_8__["getJobOffers"])(jobId);
  }
  /*.then(r => r.data)*/
  , {
    onSuccess: function onSuccess(v) {// debugger
    }
  });
};

_s(useJobOffers, "4ZpngI1uv+Uo3WQHEZmTQ5FNM+k=", false, function () {
  return [react_query__WEBPACK_IMPORTED_MODULE_4__["useQuery"]];
});

var JobOrderOffers = function JobOrderOffers(_ref) {
  _s2();

  var _jobsHook$data, _jobOffers$data, _jobOffers$data2;

  var _ref$onlyContent = _ref.onlyContent,
      onlyContent = _ref$onlyContent === void 0 ? false : _ref$onlyContent;
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();
  var jobId = Object(react__WEBPACK_IMPORTED_MODULE_2__["useMemo"])(function () {
    return parseInt(router.query.jobId);
  }, [router.asPath]);
  var jobsHook = Object(hooks_useGetJobs__WEBPACK_IMPORTED_MODULE_7__["useGetJobs"])();
  var jobs = (_jobsHook$data = jobsHook.data) === null || _jobsHook$data === void 0 ? void 0 : _jobsHook$data.data.data.results;
  var jobOffers = useJobOffers(jobId);

  var jobOrderById = function jobOrderById(id) {
    return jobs === null || jobs === void 0 ? void 0 : jobs.find(function (job) {
      return job.id == id;
    });
  };

  var Content = /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    children: (jobOffers === null || jobOffers === void 0 ? void 0 : (_jobOffers$data = jobOffers.data) === null || _jobOffers$data === void 0 ? void 0 : _jobOffers$data.length) && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_wayfindinghq_ecopros_kit_dist_components_organism_OrderListRows_OrderListRows__WEBPACK_IMPORTED_MODULE_6__["OrderListRows"], {
      orders: jobOffers === null || jobOffers === void 0 ? void 0 : (_jobOffers$data2 = jobOffers.data) === null || _jobOffers$data2 === void 0 ? void 0 : _jobOffers$data2.map(function (e) {
        var _e$creator, _e$creator$company;

        var jobDef = jobOrderById(e['jobOrderId']);
        return _objectSpread(_objectSpread(_objectSpread({
          userHiddenName: 'Sp. z o.o.'
        }, jobDef), e), {}, {
          locationText: e.city || (jobDef === null || jobDef === void 0 ? void 0 : jobDef.city) || (e === null || e === void 0 ? void 0 : (_e$creator = e.creator) === null || _e$creator === void 0 ? void 0 : (_e$creator$company = _e$creator.company) === null || _e$creator$company === void 0 ? void 0 : _e$creator$company.city),
          executionUntil: new Date(e.executionUntil).toLocaleDateString('pl'),
          renderAction: function renderAction() {
            return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
              children: ["Kwota: ", e.priceNet, "z\u0142", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("br", {}, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 49,
                columnNumber: 38
              }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
                href: "".concat(router.basePath, "/job/").concat(jobId, "/offer/").concat(e.id),
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_wayfindinghq_ecopros_kit__WEBPACK_IMPORTED_MODULE_5__["Button"], {
                  text: "Szczeg\xF3\u0142y",
                  onClick: function onClick(ev) {
                    ev.stopPropagation();
                    ev.preventDefault();
                    router.push("/job/".concat(jobId, "/offer/").concat(e.id));
                  }
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 51,
                  columnNumber: 19
                }, _this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 50,
                columnNumber: 17
              }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("br", {}, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 57,
                columnNumber: 17
              }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("select", {
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  selected: true,
                  children: "Nowy"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 59,
                  columnNumber: 19
                }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  selected: true,
                  children: "W trakcie"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 60,
                  columnNumber: 19
                }, _this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 58,
                columnNumber: 17
              }, _this)]
            }, void 0, true);
          }
        });
      })
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 35
    }, _this) || /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("b", {
      children: "Brak ofert"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 10
    }, _this) || /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_wayfindinghq_ecopros_kit_dist_components_organism_OrderListRows_OrderListRows__WEBPACK_IMPORTED_MODULE_6__["OrderListRows"], {
      orders: [{
        locationText: "Brak ofert",
        userHiddenName: ""
      }]
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 31
    }, _this)
  }, void 0, false);

  if (onlyContent) {
    return Content;
  } else {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_common_job_order_modal_template__WEBPACK_IMPORTED_MODULE_9__["default"], {
      children: Content
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 12
    }, _this);
  }
};

_s2(JobOrderOffers, "fv0zO7mzfHJ7G3CIFlv2bN5FLOE=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"], hooks_useGetJobs__WEBPACK_IMPORTED_MODULE_7__["useGetJobs"], useJobOffers];
});

_c = JobOrderOffers;
/* harmony default export */ __webpack_exports__["default"] = (JobOrderOffers);

var _c;

$RefreshReg$(_c, "JobOrderOffers");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "../../node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvam9iL1tqb2JJZF0vb2ZmZXJzLnRzeCJdLCJuYW1lcyI6WyJ1c2VKb2JPZmZlcnMiLCJqb2JJZCIsInVzZVF1ZXJ5IiwiZ2V0Sm9iT2ZmZXJzIiwib25TdWNjZXNzIiwidiIsIkpvYk9yZGVyT2ZmZXJzIiwib25seUNvbnRlbnQiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJ1c2VNZW1vIiwicGFyc2VJbnQiLCJxdWVyeSIsImFzUGF0aCIsImpvYnNIb29rIiwidXNlR2V0Sm9icyIsImpvYnMiLCJkYXRhIiwicmVzdWx0cyIsImpvYk9mZmVycyIsImpvYk9yZGVyQnlJZCIsImlkIiwiZmluZCIsImpvYiIsIkNvbnRlbnQiLCJsZW5ndGgiLCJtYXAiLCJlIiwiam9iRGVmIiwidXNlckhpZGRlbk5hbWUiLCJsb2NhdGlvblRleHQiLCJjaXR5IiwiY3JlYXRvciIsImNvbXBhbnkiLCJleGVjdXRpb25VbnRpbCIsIkRhdGUiLCJ0b0xvY2FsZURhdGVTdHJpbmciLCJyZW5kZXJBY3Rpb24iLCJwcmljZU5ldCIsImJhc2VQYXRoIiwiZXYiLCJzdG9wUHJvcGFnYXRpb24iLCJwcmV2ZW50RGVmYXVsdCIsInB1c2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUVBO0FBR0E7QUFJQTtBQUNBO0FBRUE7QUFHTyxJQUFNQSxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDQyxLQUFEO0FBQUE7O0FBQUEsU0FBV0MsNERBQVEsQ0FBc0IsQ0FBQyxLQUFELEVBQVFELEtBQVIsRUFBZSxRQUFmLENBQXRCLEVBQWdEO0FBQUEsV0FBTUUsNERBQVksQ0FBQ0YsS0FBRCxDQUFsQjtBQUFBO0FBQXlCO0FBQXpFLElBQWlHO0FBQzlJRyxhQUQ4SSxxQkFDcElDLENBRG9JLEVBQ2pJLENBQ1g7QUFDRDtBQUg2SSxHQUFqRyxDQUFuQjtBQUFBLENBQXJCOztHQUFNTCxZO1VBQTBCRSxvRDs7O0FBTWhDLElBQU1JLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsT0FBNkI7QUFBQTs7QUFBQTs7QUFBQSw4QkFBMUJDLFdBQTBCO0FBQUEsTUFBMUJBLFdBQTBCLGlDQUFaLEtBQVk7QUFDekQsTUFBTUMsTUFBTSxHQUFHQyw2REFBUyxFQUF4QjtBQUNBLE1BQU1SLEtBQUssR0FBR1MscURBQU8sQ0FBQyxZQUFNO0FBQUMsV0FBT0MsUUFBUSxDQUFDSCxNQUFNLENBQUNJLEtBQVAsQ0FBYVgsS0FBZCxDQUFmO0FBQThDLEdBQXRELEVBQXdELENBQUNPLE1BQU0sQ0FBQ0ssTUFBUixDQUF4RCxDQUFyQjtBQUVBLE1BQU1DLFFBQVEsR0FBR0MsbUVBQVUsRUFBM0I7QUFDQSxNQUFNQyxJQUFJLHFCQUFHRixRQUFRLENBQUNHLElBQVosbURBQUcsZUFBZUEsSUFBZixDQUFvQkEsSUFBcEIsQ0FBeUJDLE9BQXRDO0FBRUEsTUFBTUMsU0FBUyxHQUFHbkIsWUFBWSxDQUFDQyxLQUFELENBQTlCOztBQUVBLE1BQU1tQixZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFBQyxFQUFFO0FBQUEsV0FBSUwsSUFBSixhQUFJQSxJQUFKLHVCQUFJQSxJQUFJLENBQUVNLElBQU4sQ0FBVyxVQUFBQyxHQUFHO0FBQUEsYUFBSUEsR0FBRyxDQUFDRixFQUFKLElBQVVBLEVBQWQ7QUFBQSxLQUFkLENBQUo7QUFBQSxHQUF2Qjs7QUFDQSxNQUFNRyxPQUFPLGdCQUFHO0FBQUEsY0FHWCxDQUFBTCxTQUFTLFNBQVQsSUFBQUEsU0FBUyxXQUFULCtCQUFBQSxTQUFTLENBQUVGLElBQVgsb0VBQWlCUSxNQUFqQixrQkFBMkIscUVBQUMsNEhBQUQ7QUFBZSxZQUFNLEVBQy9DTixTQUQrQyxhQUMvQ0EsU0FEK0MsMkNBQy9DQSxTQUFTLENBQUVGLElBRG9DLHFEQUMvQyxpQkFBaUJTLEdBQWpCLENBQXFCLFVBQUNDLENBQUQsRUFBaUU7QUFBQTs7QUFDcEYsWUFBTUMsTUFBTSxHQUFHUixZQUFZLENBQUNPLENBQUMsQ0FBQyxZQUFELENBQUYsQ0FBM0I7QUFDQTtBQUNFRSx3QkFBYyxFQUFFO0FBRGxCLFdBRUtELE1BRkwsR0FHS0QsQ0FITDtBQUlFRyxzQkFBWSxFQUFFSCxDQUFDLENBQUNJLElBQUYsS0FBVUgsTUFBVixhQUFVQSxNQUFWLHVCQUFVQSxNQUFNLENBQUVHLElBQWxCLE1BQTBCSixDQUExQixhQUEwQkEsQ0FBMUIscUNBQTBCQSxDQUFDLENBQUVLLE9BQTdCLHFFQUEwQixXQUFZQyxPQUF0Qyx1REFBMEIsbUJBQXFCRixJQUEvQyxDQUpoQjtBQUtFRyx3QkFBYyxFQUFFLElBQUlDLElBQUosQ0FBU1IsQ0FBQyxDQUFDTyxjQUFYLEVBQTJCRSxrQkFBM0IsQ0FBOEMsSUFBOUMsQ0FMbEI7QUFNRUMsc0JBQVksRUFBRSx3QkFBTTtBQUNsQixnQ0FBTztBQUFBLG9DQUNHVixDQUFDLENBQUNXLFFBREwsMEJBQ2dCO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBRGhCLGVBRUw7QUFBRyxvQkFBSSxZQUFLOUIsTUFBTSxDQUFDK0IsUUFBWixrQkFBNEJ0QyxLQUE1QixvQkFBMkMwQixDQUFDLENBQUNOLEVBQTdDLENBQVA7QUFBQSx1Q0FDRSxxRUFBQyxnRUFBRDtBQUFRLHNCQUFJLEVBQUMsbUJBQWI7QUFBeUIseUJBQU8sRUFBRSxpQkFBQ21CLEVBQUQsRUFBUTtBQUN4Q0Esc0JBQUUsQ0FBQ0MsZUFBSDtBQUNBRCxzQkFBRSxDQUFDRSxjQUFIO0FBQ0FsQywwQkFBTSxDQUFDbUMsSUFBUCxnQkFBb0IxQyxLQUFwQixvQkFBbUMwQixDQUFDLENBQUNOLEVBQXJDO0FBQ0Q7QUFKRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFGSyxlQVNMO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBVEssZUFVTDtBQUFBLHdDQUNFO0FBQVEsMEJBQVEsTUFBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEseUJBREYsZUFFRTtBQUFRLDBCQUFRLE1BQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFWSztBQUFBLDRCQUFQO0FBZUQ7QUF0Qkg7QUF3QkQsT0ExQkQ7QUFEMEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQUE1QixpQkE2Qkc7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUE3QkgsaUJBNkJ3QixxRUFBQyw0SEFBRDtBQUFlLFlBQU0sRUFDM0MsQ0FDRTtBQUNFUyxvQkFBWSxFQUFFLFlBRGhCO0FBRUVELHNCQUFjLEVBQUU7QUFGbEIsT0FERjtBQURzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaENaLG1CQUFoQjs7QUFrREEsTUFBSXRCLFdBQUosRUFBaUI7QUFDZixXQUFPaUIsT0FBUDtBQUNELEdBRkQsTUFFTztBQUNMLHdCQUFPLHFFQUFDLG1GQUFEO0FBQUEsZ0JBQ0hBO0FBREc7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQUFQO0FBR0Q7QUFDRixDQW5FTTs7SUFBTWxCLGM7VUFDSUcscUQsRUFHRU0sMkQsRUFHQ2YsWTs7O0tBUFBNLGM7QUFxRUVBLDZFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2pvYi9bam9iSWRdL29mZmVycy5iMzQwNDkyOGJiYzc2NzZmNTkwNy5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlTWVtbyB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInXHJcbmltcG9ydCBMaW5rIGZyb20gJ25leHQvbGluaydcclxuaW1wb3J0IHsgdXNlUXVlcnksIHVzZVF1ZXJ5Q2xpZW50IH0gZnJvbSAncmVhY3QtcXVlcnknO1xyXG5cclxuaW1wb3J0IHtcclxuICBCdXR0b24sXHJcbn0gZnJvbSAnQHdheWZpbmRpbmdocS9lY29wcm9zLWtpdCc7XHJcbmltcG9ydCB7IE9yZGVyTGlzdFJvd3MgfSBmcm9tICdAd2F5ZmluZGluZ2hxL2Vjb3Byb3Mta2l0L2Rpc3QvY29tcG9uZW50cy9vcmdhbmlzbS9PcmRlckxpc3RSb3dzL09yZGVyTGlzdFJvd3MnO1xyXG5pbXBvcnQgeyBPcmRlckxpc3RSb3dQcm9wcyB9IGZyb20gJ0B3YXlmaW5kaW5naHEvZWNvcHJvcy1raXQvZGlzdC9jb21wb25lbnRzL21vbGVjdWxlcy9PcmRlckxpc3RSb3cvT3JkZXJMaXN0Um93LnR5cGVzJztcclxuXHJcbmltcG9ydCB7IENyZWF0ZUpvYkR0byB9IGZyb20gJy4uLy4uLy4uLy4uL2JhY2tlbmQvc3JjL2NvbXBvbmVudHMvam9iLW9yZGVyL2pvYi1vcmRlci5kdG8nO1xyXG5pbXBvcnQgeyB1c2VHZXRKb2JzIH0gZnJvbSAnaG9va3MvdXNlR2V0Sm9icyc7XHJcbmltcG9ydCB7IGdldEpvYk9mZmVycyB9IGZyb20gJ2xpYi9hcGknO1xyXG5pbXBvcnQgeyBDcmVhdGVKb2JPZmZlckR0byB9IGZyb20gJy4uLy4uLy4uLy4uL2JhY2tlbmQvc3JjL2NvbXBvbmVudHMvam9iLW9mZmVyL2pvYi1vZmZlci5kdG8nO1xyXG5pbXBvcnQgSm9iT3JkZXJUZW1wbGF0ZSBmcm9tICdAY29tcG9uZW50cy9jb21tb24vam9iLW9yZGVyLW1vZGFsL3RlbXBsYXRlJ1xyXG5pbXBvcnQgbmV4dCBmcm9tICduZXh0JztcclxuXHJcbmV4cG9ydCBjb25zdCB1c2VKb2JPZmZlcnMgPSAoam9iSWQpID0+IHVzZVF1ZXJ5PENyZWF0ZUpvYk9mZmVyRHRvW10+KFsnam9iJywgam9iSWQsICdvZmZlcnMnXSwgKCkgPT4gZ2V0Sm9iT2ZmZXJzKGpvYklkKS8qLnRoZW4ociA9PiByLmRhdGEpKi8sIHtcclxuICBvblN1Y2Nlc3Modikge1xyXG4gICAgLy8gZGVidWdnZXJcclxuICB9XHJcbn0pXHJcblxyXG5leHBvcnQgY29uc3QgSm9iT3JkZXJPZmZlcnMgPSAoeyBvbmx5Q29udGVudCA9IGZhbHNlIH0pID0+IHtcclxuICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKVxyXG4gIGNvbnN0IGpvYklkID0gdXNlTWVtbygoKSA9PiB7cmV0dXJuIHBhcnNlSW50KHJvdXRlci5xdWVyeS5qb2JJZCBhcyBzdHJpbmcpfSwgW3JvdXRlci5hc1BhdGhdKVxyXG5cclxuICBjb25zdCBqb2JzSG9vayA9IHVzZUdldEpvYnMoKVxyXG4gIGNvbnN0IGpvYnMgPSBqb2JzSG9vay5kYXRhPy5kYXRhLmRhdGEucmVzdWx0c1xyXG4gIFxyXG4gIGNvbnN0IGpvYk9mZmVycyA9IHVzZUpvYk9mZmVycyhqb2JJZClcclxuXHJcbiAgY29uc3Qgam9iT3JkZXJCeUlkID0gaWQgPT4gam9icz8uZmluZChqb2IgPT4gam9iLmlkID09IGlkKVxyXG4gIGNvbnN0IENvbnRlbnQgPSA8PlxyXG4gICAgey8qIDxwcmU+e0pTT04uc3RyaW5naWZ5KGpvYk9mZmVycy5kYXRhKX08L3ByZT4gKi99XHJcbiAgICB7XHJcbiAgICAgIChqb2JPZmZlcnM/LmRhdGE/Lmxlbmd0aCAmJiA8T3JkZXJMaXN0Um93cyBvcmRlcnM9e1xyXG4gICAgICAgIGpvYk9mZmVycz8uZGF0YT8ubWFwKChlOiBDcmVhdGVKb2JPZmZlckR0byAmIE9yZGVyTGlzdFJvd1Byb3BzKTogT3JkZXJMaXN0Um93UHJvcHMgPT4ge1xyXG4gICAgICAgICAgY29uc3Qgam9iRGVmID0gam9iT3JkZXJCeUlkKGVbJ2pvYk9yZGVySWQnXSlcclxuICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHVzZXJIaWRkZW5OYW1lOiAnU3AuIHogby5vLicsXHJcbiAgICAgICAgICAgIC4uLmpvYkRlZixcclxuICAgICAgICAgICAgLi4uZSxcclxuICAgICAgICAgICAgbG9jYXRpb25UZXh0OiBlLmNpdHkgfHwgam9iRGVmPy5jaXR5IHx8IGU/LmNyZWF0b3I/LmNvbXBhbnk/LmNpdHksXHJcbiAgICAgICAgICAgIGV4ZWN1dGlvblVudGlsOiBuZXcgRGF0ZShlLmV4ZWN1dGlvblVudGlsKS50b0xvY2FsZURhdGVTdHJpbmcoJ3BsJyksXHJcbiAgICAgICAgICAgIHJlbmRlckFjdGlvbjogKCkgPT4ge1xyXG4gICAgICAgICAgICAgIHJldHVybiA8PlxyXG4gICAgICAgICAgICAgICAgS3dvdGE6IHtlLnByaWNlTmV0fXrFgjxici8+XHJcbiAgICAgICAgICAgICAgICA8YSBocmVmPXtgJHtyb3V0ZXIuYmFzZVBhdGh9L2pvYi8ke2pvYklkfS9vZmZlci8ke2UuaWR9YH0+XHJcbiAgICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cIlN6Y3plZ8OzxYJ5XCIgb25DbGljaz17KGV2KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKClcclxuICAgICAgICAgICAgICAgICAgICBldi5wcmV2ZW50RGVmYXVsdCgpXHJcbiAgICAgICAgICAgICAgICAgICAgcm91dGVyLnB1c2goYC9qb2IvJHtqb2JJZH0vb2ZmZXIvJHtlLmlkfWApXHJcbiAgICAgICAgICAgICAgICAgIH19Lz5cclxuICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgIDxici8+XHJcbiAgICAgICAgICAgICAgICA8c2VsZWN0PlxyXG4gICAgICAgICAgICAgICAgICA8b3B0aW9uIHNlbGVjdGVkPk5vd3k8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgPG9wdGlvbiBzZWxlY3RlZD5XIHRyYWtjaWU8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG4gICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0vPilcclxuICAgICAgfHwgPGI+QnJhayBvZmVydDwvYj4gfHwgPE9yZGVyTGlzdFJvd3Mgb3JkZXJzPXtcclxuICAgICAgICBbXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGxvY2F0aW9uVGV4dDogXCJCcmFrIG9mZXJ0XCIsXHJcbiAgICAgICAgICAgIHVzZXJIaWRkZW5OYW1lOiBcIlwiLFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIF0gYXMgT3JkZXJMaXN0Um93UHJvcHNbXVxyXG4gICAgICB9Lz5cclxuICAgIH1cclxuICAgIHsvKlxyXG4gICAgPGRpdiBjbGFzc05hbWU9XCJyaWdodFwiIHN0eWxlPXt7ZmxvYXQ6ICdsZWZ0JywgbWFyZ2luOiAnMTBweCd9fT5cclxuICAgICAgPEJ1dHRvbiBjb2xvcj1cInllbGxvd1wiIHRleHQ9XCJaxYLDs8W8IG9mZXJ0xJlcIiBvbkNsaWNrPXsgKCkgPT4ge1xyXG4gICAgICAgIHJvdXRlci5wdXNoKGAvam9iLyR7am9iSWR9L2FwcGx5YClcclxuICAgICAgfX0vPlxyXG4gICAgPC9kaXY+XHJcbiAgICAqL31cclxuICA8Lz5cclxuXHJcbiAgaWYgKG9ubHlDb250ZW50KSB7XHJcbiAgICByZXR1cm4gQ29udGVudFxyXG4gIH0gZWxzZSB7XHJcbiAgICByZXR1cm4gPEpvYk9yZGVyVGVtcGxhdGU+XHJcbiAgICAgIHsgQ29udGVudCB9XHJcbiAgICA8L0pvYk9yZGVyVGVtcGxhdGU+XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBKb2JPcmRlck9mZmVyc1xyXG4iXSwic291cmNlUm9vdCI6IiJ9