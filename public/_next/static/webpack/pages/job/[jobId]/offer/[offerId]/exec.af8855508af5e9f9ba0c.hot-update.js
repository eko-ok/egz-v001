webpackHotUpdate_N_E("pages/job/[jobId]/offer/[offerId]/exec",{

/***/ "./pages/job/[jobId]/offer/templateOffer.tsx":
/*!***************************************************!*\
  !*** ./pages/job/[jobId]/offer/templateOffer.tsx ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return JobOfferTemplate; });
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "../../node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _components_common_job_order_modal_template__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @components/common/job-order-modal/template */ "./components/common/job-order-modal/template.tsx");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ "../../node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);





var _jsxFileName = "C:\\Users\\azochniak\\Desktop\\ecop\\trading-platform\\packages\\frontend\\pages\\job\\[jobId]\\offer\\templateOffer.tsx",
    _s = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }



function JobOfferTemplate(_ref) {
  _s();

  var _ref$job = _ref.job,
      job = _ref$job === void 0 ? {} : _ref$job,
      _ref$offer = _ref.offer,
      offer = _ref$offer === void 0 ? {} : _ref$offer,
      props = Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_3__["default"])(_ref, ["job", "offer"]);

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_5__["useRouter"])();
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(_components_common_job_order_modal_template__WEBPACK_IMPORTED_MODULE_4__["default"], _objectSpread({
    items: [].concat(Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_2__["default"])(0 ? undefined : []), [{
      label: 'Szczegóły zlecenia',
      href: '..',
      value: "/job/".concat(job.id)
    }, {
      label: 'Szczegóły oferty',
      href: './',
      value: "/job/".concat(job.id, "/offer/").concat(offer.id)
    }, {
      label: 'Dane firmy podwykonawcy',
      href: "./exec",
      value: "/job/".concat(job.id, "/offer/").concat(offer.id, "/exec")
    }]),
    onClick: function onClick(ev, entry) {
      ev.preventDefault();
      ev.stopPropagation(); // router.push(`/job/${job.id}/offer/${offer.id}/${entry.value !== undefined ? '/' + entry.value : ''}`)
      // if (!entry.value.includes('exec'))

      router.push(entry.value);
    }
  }, props), void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 6,
    columnNumber: 10
  }, this);
}

_s(JobOfferTemplate, "fN7XvhJ+p5oE6+Xlo0NJmXpxjC8=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_5__["useRouter"]];
});

_c = JobOfferTemplate;

var _c;

$RefreshReg$(_c, "JobOfferTemplate");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "../../node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvam9iL1tqb2JJZF0vb2ZmZXIvdGVtcGxhdGVPZmZlci50c3giXSwibmFtZXMiOlsiSm9iT2ZmZXJUZW1wbGF0ZSIsImpvYiIsIm9mZmVyIiwicHJvcHMiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJsYWJlbCIsImhyZWYiLCJ2YWx1ZSIsImlkIiwiZXYiLCJlbnRyeSIsInByZXZlbnREZWZhdWx0Iiwic3RvcFByb3BhZ2F0aW9uIiwicHVzaCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRWUsU0FBU0EsZ0JBQVQsT0FBNEQ7QUFBQTs7QUFBQSxzQkFBakNDLEdBQWlDO0FBQUEsTUFBakNBLEdBQWlDLHlCQUEzQixFQUEyQjtBQUFBLHdCQUF2QkMsS0FBdUI7QUFBQSxNQUF2QkEsS0FBdUIsMkJBQWYsRUFBZTtBQUFBLE1BQVJDLEtBQVE7O0FBQ3pFLE1BQU1DLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7QUFDQSxzQkFBTyxxRUFBQyxtRkFBRDtBQUNMLFNBQUssdUxBQ0EsSUFBSSxTQUFKLEdBSUUsRUFMRixJQU1IO0FBQ0VDLFdBQUssRUFBRSxvQkFEVDtBQUVFQyxVQUFJLEVBQUUsSUFGUjtBQUdFQyxXQUFLLGlCQUFVUCxHQUFHLENBQUNRLEVBQWQ7QUFIUCxLQU5HLEVBV0g7QUFDRUgsV0FBSyxFQUFFLGtCQURUO0FBRUVDLFVBQUksRUFBRSxJQUZSO0FBR0VDLFdBQUssaUJBQVVQLEdBQUcsQ0FBQ1EsRUFBZCxvQkFBMEJQLEtBQUssQ0FBQ08sRUFBaEM7QUFIUCxLQVhHLEVBZ0JIO0FBQ0VILFdBQUssRUFBRSx5QkFEVDtBQUVFQyxVQUFJLFVBRk47QUFHRUMsV0FBSyxpQkFBVVAsR0FBRyxDQUFDUSxFQUFkLG9CQUEwQlAsS0FBSyxDQUFDTyxFQUFoQztBQUhQLEtBaEJHLEVBREE7QUF1QkwsV0FBTyxFQUFFLGlCQUFDQyxFQUFELEVBQUtDLEtBQUwsRUFBZTtBQUN0QkQsUUFBRSxDQUFDRSxjQUFIO0FBQ0FGLFFBQUUsQ0FBQ0csZUFBSCxHQUZzQixDQUd0QjtBQUNBOztBQUNBVCxZQUFNLENBQUNVLElBQVAsQ0FBWUgsS0FBSyxDQUFDSCxLQUFsQjtBQUNEO0FBN0JJLEtBOEJITCxLQTlCRztBQUFBO0FBQUE7QUFBQTtBQUFBLFVBQVA7QUErQkQ7O0dBakN1QkgsZ0I7VUFDUEsscUQ7OztLQURPTCxnQiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9qb2IvW2pvYklkXS9vZmZlci9bb2ZmZXJJZF0vZXhlYy5hZjg4NTU1MDhhZjVlOWY5YmEwYy5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEpvYk9yZGVyVGVtcGxhdGUgZnJvbSAnQGNvbXBvbmVudHMvY29tbW9uL2pvYi1vcmRlci1tb2RhbC90ZW1wbGF0ZSdcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInXHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBKb2JPZmZlclRlbXBsYXRlKHtqb2IgPSB7fSwgb2ZmZXIgPSB7fSwgLi4ucHJvcHN9KSB7XHJcbiAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKClcclxuICByZXR1cm4gPEpvYk9yZGVyVGVtcGxhdGVcclxuICAgIGl0ZW1zPXtbXHJcbiAgICAgIC4uLjAgPyBbe1xyXG4gICAgICAgIGxhYmVsOiAnV3N6eXN0a2llIHpsZWNlbmlhJyxcclxuICAgICAgICBocmVmOiAnL2pvYicsXHJcbiAgICAgICAgdmFsdWU6ICcvam9iJ1xyXG4gICAgICB9XSA6IFtdLFxyXG4gICAgICB7XHJcbiAgICAgICAgbGFiZWw6ICdTemN6ZWfDs8WCeSB6bGVjZW5pYScsXHJcbiAgICAgICAgaHJlZjogJy4uJyxcclxuICAgICAgICB2YWx1ZTogYC9qb2IvJHtqb2IuaWR9YFxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgbGFiZWw6ICdTemN6ZWfDs8WCeSBvZmVydHknLFxyXG4gICAgICAgIGhyZWY6ICcuLycsXHJcbiAgICAgICAgdmFsdWU6IGAvam9iLyR7am9iLmlkfS9vZmZlci8ke29mZmVyLmlkfWBcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGxhYmVsOiAnRGFuZSBmaXJteSBwb2R3eWtvbmF3Y3knLFxyXG4gICAgICAgIGhyZWY6IGAuL2V4ZWNgLFxyXG4gICAgICAgIHZhbHVlOiBgL2pvYi8ke2pvYi5pZH0vb2ZmZXIvJHtvZmZlci5pZH0vZXhlY2BcclxuICAgICAgfSxcclxuICAgIF19XHJcbiAgICBvbkNsaWNrPXsoZXYsIGVudHJ5KSA9PiB7XHJcbiAgICAgIGV2LnByZXZlbnREZWZhdWx0KClcclxuICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKClcclxuICAgICAgLy8gcm91dGVyLnB1c2goYC9qb2IvJHtqb2IuaWR9L29mZmVyLyR7b2ZmZXIuaWR9LyR7ZW50cnkudmFsdWUgIT09IHVuZGVmaW5lZCA/ICcvJyArIGVudHJ5LnZhbHVlIDogJyd9YClcclxuICAgICAgLy8gaWYgKCFlbnRyeS52YWx1ZS5pbmNsdWRlcygnZXhlYycpKVxyXG4gICAgICByb3V0ZXIucHVzaChlbnRyeS52YWx1ZSlcclxuICAgIH19XHJcbiAgey4uLnByb3BzfS8+XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==