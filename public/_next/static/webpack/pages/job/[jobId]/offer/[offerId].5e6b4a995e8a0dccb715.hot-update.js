webpackHotUpdate_N_E("pages/job/[jobId]/offer/[offerId]",{

/***/ "./pages/job/[jobId]/offer/templateOffer.tsx":
/*!***************************************************!*\
  !*** ./pages/job/[jobId]/offer/templateOffer.tsx ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return JobOfferTemplate; });
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "../../node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _components_common_job_order_modal_template__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @components/common/job-order-modal/template */ "./components/common/job-order-modal/template.tsx");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ "../../node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);





var _jsxFileName = "C:\\Users\\azochniak\\Desktop\\ecop\\trading-platform\\packages\\frontend\\pages\\job\\[jobId]\\offer\\templateOffer.tsx",
    _s = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }



function JobOfferTemplate(_ref) {
  _s();

  var _ref$job = _ref.job,
      job = _ref$job === void 0 ? {} : _ref$job,
      _ref$offer = _ref.offer,
      offer = _ref$offer === void 0 ? {} : _ref$offer,
      props = Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_3__["default"])(_ref, ["job", "offer"]);

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_5__["useRouter"])();
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(_components_common_job_order_modal_template__WEBPACK_IMPORTED_MODULE_4__["default"], _objectSpread({
    items: [].concat(Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_2__["default"])(0 ? undefined : []), [{
      label: 'Szczegóły zlecenia',
      href: '..',
      value: "/job/".concat(job.id)
    }, {
      label: 'Szczegóły oferty',
      href: './',
      value: "/job/".concat(job.id, "/offer/").concat(offer.id)
    }, {
      label: 'Dane firmy podwykonawcy',
      href: "./exec",
      value: "/job/".concat(job.id, "/offer/").concat(offer.id, "/exec")
    }]),
    onClick: function onClick(ev, entry) {
      ev.preventDefault();
      ev.stopPropagation(); // router.push(`/job/${job.id}/offer/${offer.id}/${entry.value !== undefined ? '/' + entry.value : ''}`)

      if (!entry.value.includes('exec')) {
        router.push(entry.value);
      }
    }
  }, props), void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 6,
    columnNumber: 10
  }, this);
}

_s(JobOfferTemplate, "fN7XvhJ+p5oE6+Xlo0NJmXpxjC8=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_5__["useRouter"]];
});

_c = JobOfferTemplate;

var _c;

$RefreshReg$(_c, "JobOfferTemplate");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "../../node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvam9iL1tqb2JJZF0vb2ZmZXIvdGVtcGxhdGVPZmZlci50c3giXSwibmFtZXMiOlsiSm9iT2ZmZXJUZW1wbGF0ZSIsImpvYiIsIm9mZmVyIiwicHJvcHMiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJsYWJlbCIsImhyZWYiLCJ2YWx1ZSIsImlkIiwiZXYiLCJlbnRyeSIsInByZXZlbnREZWZhdWx0Iiwic3RvcFByb3BhZ2F0aW9uIiwiaW5jbHVkZXMiLCJwdXNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFZSxTQUFTQSxnQkFBVCxPQUE0RDtBQUFBOztBQUFBLHNCQUFqQ0MsR0FBaUM7QUFBQSxNQUFqQ0EsR0FBaUMseUJBQTNCLEVBQTJCO0FBQUEsd0JBQXZCQyxLQUF1QjtBQUFBLE1BQXZCQSxLQUF1QiwyQkFBZixFQUFlO0FBQUEsTUFBUkMsS0FBUTs7QUFDekUsTUFBTUMsTUFBTSxHQUFHQyw2REFBUyxFQUF4QjtBQUNBLHNCQUFPLHFFQUFDLG1GQUFEO0FBQ0wsU0FBSyx1TEFDQSxJQUFJLFNBQUosR0FJRSxFQUxGLElBTUg7QUFDRUMsV0FBSyxFQUFFLG9CQURUO0FBRUVDLFVBQUksRUFBRSxJQUZSO0FBR0VDLFdBQUssaUJBQVVQLEdBQUcsQ0FBQ1EsRUFBZDtBQUhQLEtBTkcsRUFXSDtBQUNFSCxXQUFLLEVBQUUsa0JBRFQ7QUFFRUMsVUFBSSxFQUFFLElBRlI7QUFHRUMsV0FBSyxpQkFBVVAsR0FBRyxDQUFDUSxFQUFkLG9CQUEwQlAsS0FBSyxDQUFDTyxFQUFoQztBQUhQLEtBWEcsRUFnQkg7QUFDRUgsV0FBSyxFQUFFLHlCQURUO0FBRUVDLFVBQUksVUFGTjtBQUdFQyxXQUFLLGlCQUFVUCxHQUFHLENBQUNRLEVBQWQsb0JBQTBCUCxLQUFLLENBQUNPLEVBQWhDO0FBSFAsS0FoQkcsRUFEQTtBQXVCTCxXQUFPLEVBQUUsaUJBQUNDLEVBQUQsRUFBS0MsS0FBTCxFQUFlO0FBQ3RCRCxRQUFFLENBQUNFLGNBQUg7QUFDQUYsUUFBRSxDQUFDRyxlQUFILEdBRnNCLENBR3RCOztBQUNBLFVBQUksQ0FBQ0YsS0FBSyxDQUFDSCxLQUFOLENBQVlNLFFBQVosQ0FBcUIsTUFBckIsQ0FBTCxFQUFtQztBQUNqQ1YsY0FBTSxDQUFDVyxJQUFQLENBQVlKLEtBQUssQ0FBQ0gsS0FBbEI7QUFDRDtBQUNGO0FBOUJJLEtBK0JITCxLQS9CRztBQUFBO0FBQUE7QUFBQTtBQUFBLFVBQVA7QUFnQ0Q7O0dBbEN1QkgsZ0I7VUFDUEsscUQ7OztLQURPTCxnQiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9qb2IvW2pvYklkXS9vZmZlci9bb2ZmZXJJZF0uNWU2YjRhOTk1ZThhMGRjY2I3MTUuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBKb2JPcmRlclRlbXBsYXRlIGZyb20gJ0Bjb21wb25lbnRzL2NvbW1vbi9qb2Itb3JkZXItbW9kYWwvdGVtcGxhdGUnXHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJ1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSm9iT2ZmZXJUZW1wbGF0ZSh7am9iID0ge30sIG9mZmVyID0ge30sIC4uLnByb3BzfSkge1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpXHJcbiAgcmV0dXJuIDxKb2JPcmRlclRlbXBsYXRlXHJcbiAgICBpdGVtcz17W1xyXG4gICAgICAuLi4wID8gW3tcclxuICAgICAgICBsYWJlbDogJ1dzenlzdGtpZSB6bGVjZW5pYScsXHJcbiAgICAgICAgaHJlZjogJy9qb2InLFxyXG4gICAgICAgIHZhbHVlOiAnL2pvYidcclxuICAgICAgfV0gOiBbXSxcclxuICAgICAge1xyXG4gICAgICAgIGxhYmVsOiAnU3pjemVnw7PFgnkgemxlY2VuaWEnLFxyXG4gICAgICAgIGhyZWY6ICcuLicsXHJcbiAgICAgICAgdmFsdWU6IGAvam9iLyR7am9iLmlkfWBcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGxhYmVsOiAnU3pjemVnw7PFgnkgb2ZlcnR5JyxcclxuICAgICAgICBocmVmOiAnLi8nLFxyXG4gICAgICAgIHZhbHVlOiBgL2pvYi8ke2pvYi5pZH0vb2ZmZXIvJHtvZmZlci5pZH1gXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBsYWJlbDogJ0RhbmUgZmlybXkgcG9kd3lrb25hd2N5JyxcclxuICAgICAgICBocmVmOiBgLi9leGVjYCxcclxuICAgICAgICB2YWx1ZTogYC9qb2IvJHtqb2IuaWR9L29mZmVyLyR7b2ZmZXIuaWR9L2V4ZWNgXHJcbiAgICAgIH0sXHJcbiAgICBdfVxyXG4gICAgb25DbGljaz17KGV2LCBlbnRyeSkgPT4ge1xyXG4gICAgICBldi5wcmV2ZW50RGVmYXVsdCgpXHJcbiAgICAgIGV2LnN0b3BQcm9wYWdhdGlvbigpXHJcbiAgICAgIC8vIHJvdXRlci5wdXNoKGAvam9iLyR7am9iLmlkfS9vZmZlci8ke29mZmVyLmlkfS8ke2VudHJ5LnZhbHVlICE9PSB1bmRlZmluZWQgPyAnLycgKyBlbnRyeS52YWx1ZSA6ICcnfWApXHJcbiAgICAgIGlmICghZW50cnkudmFsdWUuaW5jbHVkZXMoJ2V4ZWMnKSkge1xyXG4gICAgICAgIHJvdXRlci5wdXNoKGVudHJ5LnZhbHVlKVxyXG4gICAgICB9XHJcbiAgICB9fVxyXG4gIHsuLi5wcm9wc30vPlxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=