webpackHotUpdate_N_E("pages/job/[jobId]/offer/[offerId]",{

/***/ "./pages/job/[jobId]/offer/[offerId]/index.tsx":
/*!*****************************************************!*\
  !*** ./pages/job/[jobId]/offer/[offerId]/index.tsx ***!
  \*****************************************************/
/*! exports provided: JobOrderOfferDetails, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobOrderOfferDetails", function() { return JobOrderOfferDetails; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "../../node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ "../../node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "../../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-query */ "../../node_modules/react-query/es/index.js");
/* harmony import */ var _templateOffer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../templateOffer */ "./pages/job/[jobId]/offer/templateOffer.tsx");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! styled-components */ "../../node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var _wayfindinghq_ecopros_kit__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wayfindinghq/ecopros-kit */ "../../node_modules/@wayfindinghq/ecopros-kit/index.js");
/* harmony import */ var lib_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lib/api */ "./lib/api.ts");
/* harmony import */ var _components_common_job_offer_details__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @components/common/job-offer-details */ "./components/common/job-offer-details/index.tsx");


var _jsxFileName = "C:\\Users\\azochniak\\Desktop\\ecop\\trading-platform\\packages\\frontend\\pages\\job\\[jobId]\\offer\\[offerId]\\index.tsx",
    _s = $RefreshSig$();









var Card = styled_components__WEBPACK_IMPORTED_MODULE_5__["default"].div.withConfig({
  displayName: "offerId__Card",
  componentId: "zjacve-0"
})(["background:white;border-radius:20px;padding:20px;"]);
_c = Card;
function JobOrderOfferDetails() {
  _s();

  var _jobOffers$data, _jobOffers$data2;

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_1__["useRouter"])();
  var jobId = Object(react__WEBPACK_IMPORTED_MODULE_2__["useMemo"])(function () {
    return parseInt(router.query.jobId);
  }, [router.asPath]);
  var offerId = Object(react__WEBPACK_IMPORTED_MODULE_2__["useMemo"])(function () {
    return parseInt(router.query.offerId);
  }, [router.asPath]);
  var jobOffers = Object(react_query__WEBPACK_IMPORTED_MODULE_3__["useQuery"])(['job', jobId, 'offers'
  /*, offerId*/
  ], function () {
    return Object(lib_api__WEBPACK_IMPORTED_MODULE_7__["getJobOffers"])(jobId);
  }
  /*.then(r => r.data)*/
  , {
    initialData: [],
    onSuccess: function onSuccess(v) {
      debugger;
    }
  });
  console.log('info', ['jobid', jobId, 'offerid', offerId], jobOffers === null || jobOffers === void 0 ? void 0 : jobOffers.data);
  var offersDet = jobOffers === null || jobOffers === void 0 ? void 0 : (_jobOffers$data = jobOffers.data) === null || _jobOffers$data === void 0 ? void 0 : _jobOffers$data.find(function (e) {
    return e.id == offerId;
  });
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_templateOffer__WEBPACK_IMPORTED_MODULE_4__["default"], {
    job: jobOffers === null || jobOffers === void 0 ? void 0 : (_jobOffers$data2 = jobOffers.data) === null || _jobOffers$data2 === void 0 ? void 0 : _jobOffers$data2.find(function (e) {
      return e.id == jobId;
    }),
    offer: offersDet,
    selectedValue: "offerDetails",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(Card, {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_wayfindinghq_ecopros_kit__WEBPACK_IMPORTED_MODULE_6__["ProductSpecification"], {
          title: "Szczeg\xF3\u0142y oferty",
          items: offersDet ? [{
            label: 'ID',
            value: "#".concat(offersDet.id)
          }, {
            label: 'Cena',
            value: offersDet.priceNet
          }, {
            label: 'Realizacja do',
            value: new Date(offersDet.executionUntil).toLocaleDateString('pl')
          }, {
            label: 'Telefon',
            value: offersDet.phoneNumber
          }, {
            label: 'Informacje',
            value: offersDet.additionalInfo
          }] : []
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 46,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 45,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        style: {
          marginTop: '600px'
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_common_job_offer_details__WEBPACK_IMPORTED_MODULE_8__["default"], {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 73,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 37,
    columnNumber: 5
  }, this);
}

_s(JobOrderOfferDetails, "BALXCrgV84V6bPZVXdH68AejN1I=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_1__["useRouter"], react_query__WEBPACK_IMPORTED_MODULE_3__["useQuery"]];
});

_c2 = JobOrderOfferDetails;
/* harmony default export */ __webpack_exports__["default"] = (JobOrderOfferDetails);

var _c, _c2;

$RefreshReg$(_c, "Card");
$RefreshReg$(_c2, "JobOrderOfferDetails");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "../../node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvam9iL1tqb2JJZF0vb2ZmZXIvW29mZmVySWRdL2luZGV4LnRzeCJdLCJuYW1lcyI6WyJDYXJkIiwic3R5bGVkIiwiZGl2IiwiSm9iT3JkZXJPZmZlckRldGFpbHMiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJqb2JJZCIsInVzZU1lbW8iLCJwYXJzZUludCIsInF1ZXJ5IiwiYXNQYXRoIiwib2ZmZXJJZCIsImpvYk9mZmVycyIsInVzZVF1ZXJ5IiwiZ2V0Sm9iT2ZmZXJzIiwiaW5pdGlhbERhdGEiLCJvblN1Y2Nlc3MiLCJ2IiwiY29uc29sZSIsImxvZyIsImRhdGEiLCJvZmZlcnNEZXQiLCJmaW5kIiwiZSIsImlkIiwibGFiZWwiLCJ2YWx1ZSIsInByaWNlTmV0IiwiRGF0ZSIsImV4ZWN1dGlvblVudGlsIiwidG9Mb2NhbGVEYXRlU3RyaW5nIiwicGhvbmVOdW1iZXIiLCJhZGRpdGlvbmFsSW5mbyIsIm1hcmdpblRvcCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFFQTtBQUNBO0FBRUEsSUFBTUEsSUFBSSxHQUFHQyx5REFBTSxDQUFDQyxHQUFWO0FBQUE7QUFBQTtBQUFBLHlEQUFWO0tBQU1GLEk7QUFNQyxTQUFTRyxvQkFBVCxHQUE4QztBQUFBOztBQUFBOztBQUNuRCxNQUFNQyxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCO0FBQ0EsTUFBTUMsS0FBSyxHQUFHQyxxREFBTyxDQUFDLFlBQU07QUFBQyxXQUFPQyxRQUFRLENBQUNKLE1BQU0sQ0FBQ0ssS0FBUCxDQUFhSCxLQUFkLENBQWY7QUFBOEMsR0FBdEQsRUFBd0QsQ0FBQ0YsTUFBTSxDQUFDTSxNQUFSLENBQXhELENBQXJCO0FBQ0EsTUFBTUMsT0FBTyxHQUFHSixxREFBTyxDQUFDLFlBQU07QUFBQyxXQUFPQyxRQUFRLENBQUNKLE1BQU0sQ0FBQ0ssS0FBUCxDQUFhRSxPQUFkLENBQWY7QUFBZ0QsR0FBeEQsRUFBMEQsQ0FBQ1AsTUFBTSxDQUFDTSxNQUFSLENBQTFELENBQXZCO0FBRUEsTUFBTUUsU0FBUyxHQUFHQyw0REFBUSxDQUFDLENBQUMsS0FBRCxFQUFRUCxLQUFSLEVBQWU7QUFBUTtBQUF2QixHQUFELEVBQXdDO0FBQUEsV0FBTVEsNERBQVksQ0FBQ1IsS0FBRCxDQUFsQjtBQUFBO0FBQXlCO0FBQWpFLElBQXlGO0FBQ2pIUyxlQUFXLEVBQUUsRUFEb0c7QUFFakhDLGFBRmlILHFCQUV2R0MsQ0FGdUcsRUFFcEc7QUFDWDtBQUNEO0FBSmdILEdBQXpGLENBQTFCO0FBTUFDLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLE1BQVosRUFBb0IsQ0FBQyxPQUFELEVBQVViLEtBQVYsRUFBaUIsU0FBakIsRUFBNEJLLE9BQTVCLENBQXBCLEVBQTBEQyxTQUExRCxhQUEwREEsU0FBMUQsdUJBQTBEQSxTQUFTLENBQUVRLElBQXJFO0FBRUEsTUFBSUMsU0FBUyxHQUFJVCxTQUFKLGFBQUlBLFNBQUosMENBQUlBLFNBQVMsQ0FBRVEsSUFBZixvREFBRyxnQkFBcURFLElBQXJELENBQTBELFVBQUFDLENBQUM7QUFBQSxXQUFJQSxDQUFDLENBQUNDLEVBQUYsSUFBUWIsT0FBWjtBQUFBLEdBQTNELENBQWhCO0FBRUEsc0JBQ0UscUVBQUMsc0RBQUQ7QUFDRSxPQUFHLEVBQUVDLFNBQUYsYUFBRUEsU0FBRiwyQ0FBRUEsU0FBUyxDQUFFUSxJQUFiLHFEQUFFLGlCQUFpQkUsSUFBakIsQ0FBc0IsVUFBQUMsQ0FBQztBQUFBLGFBQUlBLENBQUMsQ0FBQ0MsRUFBRixJQUFRbEIsS0FBWjtBQUFBLEtBQXZCLENBRFA7QUFFRSxTQUFLLEVBQUVlLFNBRlQ7QUFHRSxpQkFBYSxFQUFDLGNBSGhCO0FBQUEsMkJBSUU7QUFBQSw4QkFJRSxxRUFBQyxJQUFEO0FBQUEsK0JBQ0UscUVBQUMsOEVBQUQ7QUFDRSxlQUFLLEVBQUMsMEJBRFI7QUFFRSxlQUFLLEVBQUVBLFNBQVMsR0FBRyxDQUNqQjtBQUNFSSxpQkFBSyxFQUFFLElBRFQ7QUFFRUMsaUJBQUssYUFBTUwsU0FBUyxDQUFDRyxFQUFoQjtBQUZQLFdBRGlCLEVBS2pCO0FBQ0VDLGlCQUFLLEVBQUUsTUFEVDtBQUVFQyxpQkFBSyxFQUFFTCxTQUFTLENBQUNNO0FBRm5CLFdBTGlCLEVBU2pCO0FBQ0VGLGlCQUFLLEVBQUUsZUFEVDtBQUVFQyxpQkFBSyxFQUFFLElBQUlFLElBQUosQ0FBU1AsU0FBUyxDQUFDUSxjQUFuQixFQUFtQ0Msa0JBQW5DLENBQXNELElBQXREO0FBRlQsV0FUaUIsRUFhakI7QUFDRUwsaUJBQUssRUFBRSxTQURUO0FBRUVDLGlCQUFLLEVBQUVMLFNBQVMsQ0FBQ1U7QUFGbkIsV0FiaUIsRUFpQmpCO0FBQ0VOLGlCQUFLLEVBQUUsWUFEVDtBQUVFQyxpQkFBSyxFQUFFTCxTQUFTLENBQUNXO0FBRm5CLFdBakJpQixDQUFILEdBcUJaO0FBdkJOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBSkYsZUErQkU7QUFBSyxhQUFLLEVBQUU7QUFBQ0MsbUJBQVMsRUFBRTtBQUFaLFNBQVo7QUFBQSwrQkFDRSxxRUFBQyw0RUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQS9CRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKRjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUEyQ0Q7O0dBMURlOUIsb0I7VUFDQ0UscUQsRUFJR1Esb0Q7OztNQUxKVixvQjtBQTREREEsbUZBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvam9iL1tqb2JJZF0vb2ZmZXIvW29mZmVySWRdLjYyZDdiOGI4YjVkZjI0NzIwNTU5LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBKb2JPcmRlck1vZGFsIH0gZnJvbSAnQGNvbXBvbmVudHMvY29tbW9uL2pvYi1vcmRlci1tb2RhbCdcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInXHJcbmltcG9ydCB7IFJlYWN0RWxlbWVudCwgdXNlTWVtbyB9IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgeyB1c2VRdWVyeSB9IGZyb20gJ3JlYWN0LXF1ZXJ5J1xyXG5pbXBvcnQgSm9iT2ZmZXJUZW1wbGF0ZSBmcm9tICcuLi90ZW1wbGF0ZU9mZmVyJ1xyXG5cclxuaW1wb3J0IHsgQ3JlYXRlSm9iT2ZmZXJEdG8gfSBmcm9tICcuLi8uLi8uLi8uLi8uLi8uLi9iYWNrZW5kL3NyYy9jb21wb25lbnRzL2pvYi1vZmZlci9qb2Itb2ZmZXIuZHRvJ1xyXG5pbXBvcnQgeyBGb3JtUHJvdmlkZXIsIHVzZUZvcm0gfSBmcm9tICdyZWFjdC1ob29rLWZvcm0nXHJcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnXHJcbmltcG9ydCB7IFByb2R1Y3RTcGVjaWZpY2F0aW9uIH0gZnJvbSAnQHdheWZpbmRpbmdocS9lY29wcm9zLWtpdCdcclxuaW1wb3J0IHsgU3BlY2lmaWNhdGlvbkl0ZW0gfSBmcm9tICdAd2F5ZmluZGluZ2hxL2Vjb3Byb3Mta2l0L2Rpc3QvY29tcG9uZW50cy9tb2xlY3VsZXMvUHJvZHVjdC9Qcm9kdWN0U3BlY2lmaWNhdGlvbi9Qcm9kdWN0U3BlY2lmaWNhdGlvbi50eXBlcydcclxuaW1wb3J0IHsgZ2V0Sm9iT2ZmZXJzIH0gZnJvbSAnbGliL2FwaSdcclxuaW1wb3J0IEpvYk9mZmVyRGV0YWlscyBmcm9tICdAY29tcG9uZW50cy9jb21tb24vam9iLW9mZmVyLWRldGFpbHMnXHJcblxyXG5jb25zdCBDYXJkID0gc3R5bGVkLmRpdmBcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbmBcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBKb2JPcmRlck9mZmVyRGV0YWlscygpOiBSZWFjdEVsZW1lbnQge1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpXHJcbiAgY29uc3Qgam9iSWQgPSB1c2VNZW1vKCgpID0+IHtyZXR1cm4gcGFyc2VJbnQocm91dGVyLnF1ZXJ5LmpvYklkIGFzIHN0cmluZyl9LCBbcm91dGVyLmFzUGF0aF0pXHJcbiAgY29uc3Qgb2ZmZXJJZCA9IHVzZU1lbW8oKCkgPT4ge3JldHVybiBwYXJzZUludChyb3V0ZXIucXVlcnkub2ZmZXJJZCBhcyBzdHJpbmcpfSwgW3JvdXRlci5hc1BhdGhdKVxyXG4gIFxyXG4gIGNvbnN0IGpvYk9mZmVycyA9IHVzZVF1ZXJ5KFsnam9iJywgam9iSWQsICdvZmZlcnMnLyosIG9mZmVySWQqL10sICgpID0+IGdldEpvYk9mZmVycyhqb2JJZCkvKi50aGVuKHIgPT4gci5kYXRhKSovLCB7XHJcbiAgICBpbml0aWFsRGF0YTogW10sXHJcbiAgICBvblN1Y2Nlc3Modikge1xyXG4gICAgICBkZWJ1Z2dlclxyXG4gICAgfVxyXG4gIH0pXHJcbiAgY29uc29sZS5sb2coJ2luZm8nLCBbJ2pvYmlkJywgam9iSWQsICdvZmZlcmlkJywgb2ZmZXJJZF0sIGpvYk9mZmVycz8uZGF0YSlcclxuXHJcbiAgbGV0IG9mZmVyc0RldCA9IChqb2JPZmZlcnM/LmRhdGEgYXMgdW5rbm93biBhcyBDcmVhdGVKb2JPZmZlckR0b1tdKT8uZmluZChlID0+IGUuaWQgPT0gb2ZmZXJJZClcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxKb2JPZmZlclRlbXBsYXRlIFxyXG4gICAgICBqb2I9e2pvYk9mZmVycz8uZGF0YT8uZmluZChlID0+IGUuaWQgPT0gam9iSWQpfVxyXG4gICAgICBvZmZlcj17b2ZmZXJzRGV0fVxyXG4gICAgICBzZWxlY3RlZFZhbHVlPSdvZmZlckRldGFpbHMnPlxyXG4gICAgICA8ZGl2PlxyXG4gICAgICAgIHsvKiA8aDE+U3pjemVnw7PFgnkgb2ZlcnR5ITwvaDE+ICovfVxyXG4gICAgICAgIHsvKiA8c21hbGw+e29mZmVyc0RldD8uY3JlYXRlZEF0fTwvc21hbGw+ICovfVxyXG4gICAgICAgIHsvKiA8cHJlPntKU09OLnN0cmluZ2lmeShqb2JPZmZlcnMsIG51bGwsIDIpfTwvcHJlPiAqL31cclxuICAgICAgICA8Q2FyZD5cclxuICAgICAgICAgIDxQcm9kdWN0U3BlY2lmaWNhdGlvblxyXG4gICAgICAgICAgICB0aXRsZT1cIlN6Y3plZ8OzxYJ5IG9mZXJ0eVwiXHJcbiAgICAgICAgICAgIGl0ZW1zPXtvZmZlcnNEZXQgPyBbXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdJRCcsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogYCMke29mZmVyc0RldC5pZH1gLFxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdDZW5hJyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiBvZmZlcnNEZXQucHJpY2VOZXQsXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlYWxpemFjamEgZG8nLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IG5ldyBEYXRlKG9mZmVyc0RldC5leGVjdXRpb25VbnRpbCkudG9Mb2NhbGVEYXRlU3RyaW5nKCdwbCcpLFxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdUZWxlZm9uJyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiBvZmZlcnNEZXQucGhvbmVOdW1iZXIsXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0luZm9ybWFjamUnLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IG9mZmVyc0RldC5hZGRpdGlvbmFsSW5mbyxcclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBdIDogW119XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9DYXJkPlxyXG4gICAgICAgIDxkaXYgc3R5bGU9e3ttYXJnaW5Ub3A6ICc2MDBweCd9fT5cclxuICAgICAgICAgIDxKb2JPZmZlckRldGFpbHMvPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIHsvKiA8SW5zdGFsbGF0aW9uRm9ybS8+ICovfVxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvSm9iT2ZmZXJUZW1wbGF0ZT5cclxuICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEpvYk9yZGVyT2ZmZXJEZXRhaWxzXHJcbiJdLCJzb3VyY2VSb290IjoiIn0=