webpackHotUpdate_N_E("pages/job/[jobId]/offer/[offerId]",{

/***/ "./pages/job/[jobId]/offer/templateOffer.tsx":
/*!***************************************************!*\
  !*** ./pages/job/[jobId]/offer/templateOffer.tsx ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return JobOfferTemplate; });
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "../../node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _components_common_job_order_modal_template__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @components/common/job-order-modal/template */ "./components/common/job-order-modal/template.tsx");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/router */ "../../node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);




var _jsxFileName = "C:\\Users\\azochniak\\Desktop\\ecop\\trading-platform\\packages\\frontend\\pages\\job\\[jobId]\\offer\\templateOffer.tsx",
    _s = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }



function JobOfferTemplate(_ref) {
  _s();

  var _ref$job = _ref.job,
      job = _ref$job === void 0 ? {} : _ref$job,
      _ref$offer = _ref.offer,
      offer = _ref$offer === void 0 ? {} : _ref$offer,
      props = Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__["default"])(_ref, ["job", "offer"]);

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_4__["useRouter"])();
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(_components_common_job_order_modal_template__WEBPACK_IMPORTED_MODULE_3__["default"], _objectSpread({
    items: [0 ? undefined : {}].concat([{
      label: 'Szczegóły zlecenia',
      href: '..',
      value: "/job/".concat(job.id)
    }, {
      label: 'Szczegóły oferty',
      href: './',
      value: "/job/".concat(job.id, "/offer/").concat(offer.id)
    }, {
      label: 'Dane firmy podwykonawcy',
      href: "./exec",
      value: "/job/".concat(job.id, "/offer/").concat(offer.id, "/exec")
    }]),
    onClick: function onClick(ev, entry) {
      ev.preventDefault();
      ev.stopPropagation(); // router.push(`/job/${job.id}/offer/${offer.id}/${entry.value !== undefined ? '/' + entry.value : ''}`)

      if (!entry.value.includes('exec')) {
        router.push(entry.value);
      }
    }
  }, props), void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 6,
    columnNumber: 10
  }, this);
}

_s(JobOfferTemplate, "fN7XvhJ+p5oE6+Xlo0NJmXpxjC8=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_4__["useRouter"]];
});

_c = JobOfferTemplate;

var _c;

$RefreshReg$(_c, "JobOfferTemplate");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "../../node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvam9iL1tqb2JJZF0vb2ZmZXIvdGVtcGxhdGVPZmZlci50c3giXSwibmFtZXMiOlsiSm9iT2ZmZXJUZW1wbGF0ZSIsImpvYiIsIm9mZmVyIiwicHJvcHMiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJsYWJlbCIsImhyZWYiLCJ2YWx1ZSIsImlkIiwiZXYiLCJlbnRyeSIsInByZXZlbnREZWZhdWx0Iiwic3RvcFByb3BhZ2F0aW9uIiwiaW5jbHVkZXMiLCJwdXNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRWUsU0FBU0EsZ0JBQVQsT0FBNEQ7QUFBQTs7QUFBQSxzQkFBakNDLEdBQWlDO0FBQUEsTUFBakNBLEdBQWlDLHlCQUEzQixFQUEyQjtBQUFBLHdCQUF2QkMsS0FBdUI7QUFBQSxNQUF2QkEsS0FBdUIsMkJBQWYsRUFBZTtBQUFBLE1BQVJDLEtBQVE7O0FBQ3pFLE1BQU1DLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7QUFDQSxzQkFBTyxxRUFBQyxtRkFBRDtBQUNMLFNBQUssRUFDQSxDQUFDLElBQUksU0FBSixHQUlBLEVBSkQsQ0FEQSxTQU1IO0FBQ0VDLFdBQUssRUFBRSxvQkFEVDtBQUVFQyxVQUFJLEVBQUUsSUFGUjtBQUdFQyxXQUFLLGlCQUFVUCxHQUFHLENBQUNRLEVBQWQ7QUFIUCxLQU5HLEVBV0g7QUFDRUgsV0FBSyxFQUFFLGtCQURUO0FBRUVDLFVBQUksRUFBRSxJQUZSO0FBR0VDLFdBQUssaUJBQVVQLEdBQUcsQ0FBQ1EsRUFBZCxvQkFBMEJQLEtBQUssQ0FBQ08sRUFBaEM7QUFIUCxLQVhHLEVBZ0JIO0FBQ0VILFdBQUssRUFBRSx5QkFEVDtBQUVFQyxVQUFJLFVBRk47QUFHRUMsV0FBSyxpQkFBVVAsR0FBRyxDQUFDUSxFQUFkLG9CQUEwQlAsS0FBSyxDQUFDTyxFQUFoQztBQUhQLEtBaEJHLEVBREE7QUF1QkwsV0FBTyxFQUFFLGlCQUFDQyxFQUFELEVBQUtDLEtBQUwsRUFBZTtBQUN0QkQsUUFBRSxDQUFDRSxjQUFIO0FBQ0FGLFFBQUUsQ0FBQ0csZUFBSCxHQUZzQixDQUd0Qjs7QUFDQSxVQUFJLENBQUNGLEtBQUssQ0FBQ0gsS0FBTixDQUFZTSxRQUFaLENBQXFCLE1BQXJCLENBQUwsRUFBbUM7QUFDakNWLGNBQU0sQ0FBQ1csSUFBUCxDQUFZSixLQUFLLENBQUNILEtBQWxCO0FBQ0Q7QUFDRjtBQTlCSSxLQStCSEwsS0EvQkc7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUFQO0FBZ0NEOztHQWxDdUJILGdCO1VBQ1BLLHFEOzs7S0FET0wsZ0IiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvam9iL1tqb2JJZF0vb2ZmZXIvW29mZmVySWRdLjYzMWE0MjI2YTYwYzBiZjEyMTVlLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgSm9iT3JkZXJUZW1wbGF0ZSBmcm9tICdAY29tcG9uZW50cy9jb21tb24vam9iLW9yZGVyLW1vZGFsL3RlbXBsYXRlJ1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tICduZXh0L3JvdXRlcidcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEpvYk9mZmVyVGVtcGxhdGUoe2pvYiA9IHt9LCBvZmZlciA9IHt9LCAuLi5wcm9wc30pIHtcclxuICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKVxyXG4gIHJldHVybiA8Sm9iT3JkZXJUZW1wbGF0ZVxyXG4gICAgaXRlbXM9e1tcclxuICAgICAgLi4uWzAgPyB7XHJcbiAgICAgICAgbGFiZWw6ICdXc3p5c3RraWUgemxlY2VuaWEnLFxyXG4gICAgICAgIGhyZWY6ICcvam9iJyxcclxuICAgICAgICB2YWx1ZTogJy9qb2InXHJcbiAgICAgIH0gOiB7fV0sXHJcbiAgICAgIHtcclxuICAgICAgICBsYWJlbDogJ1N6Y3plZ8OzxYJ5IHpsZWNlbmlhJyxcclxuICAgICAgICBocmVmOiAnLi4nLFxyXG4gICAgICAgIHZhbHVlOiBgL2pvYi8ke2pvYi5pZH1gXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBsYWJlbDogJ1N6Y3plZ8OzxYJ5IG9mZXJ0eScsXHJcbiAgICAgICAgaHJlZjogJy4vJyxcclxuICAgICAgICB2YWx1ZTogYC9qb2IvJHtqb2IuaWR9L29mZmVyLyR7b2ZmZXIuaWR9YFxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgbGFiZWw6ICdEYW5lIGZpcm15IHBvZHd5a29uYXdjeScsXHJcbiAgICAgICAgaHJlZjogYC4vZXhlY2AsXHJcbiAgICAgICAgdmFsdWU6IGAvam9iLyR7am9iLmlkfS9vZmZlci8ke29mZmVyLmlkfS9leGVjYFxyXG4gICAgICB9LFxyXG4gICAgXX1cclxuICAgIG9uQ2xpY2s9eyhldiwgZW50cnkpID0+IHtcclxuICAgICAgZXYucHJldmVudERlZmF1bHQoKVxyXG4gICAgICBldi5zdG9wUHJvcGFnYXRpb24oKVxyXG4gICAgICAvLyByb3V0ZXIucHVzaChgL2pvYi8ke2pvYi5pZH0vb2ZmZXIvJHtvZmZlci5pZH0vJHtlbnRyeS52YWx1ZSAhPT0gdW5kZWZpbmVkID8gJy8nICsgZW50cnkudmFsdWUgOiAnJ31gKVxyXG4gICAgICBpZiAoIWVudHJ5LnZhbHVlLmluY2x1ZGVzKCdleGVjJykpIHtcclxuICAgICAgICByb3V0ZXIucHVzaChlbnRyeS52YWx1ZSlcclxuICAgICAgfVxyXG4gICAgfX1cclxuICB7Li4ucHJvcHN9Lz5cclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9