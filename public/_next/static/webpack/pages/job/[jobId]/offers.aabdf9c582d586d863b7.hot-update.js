webpackHotUpdate_N_E("pages/job/[jobId]/offers",{

/***/ "./lib/mockaxios.ts":
/*!**************************!*\
  !*** ./lib/mockaxios.ts ***!
  \**************************/
/*! exports provided: apiInstance */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "apiInstance", function() { return apiInstance; });
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

// export interface AxiosInstance2 {
//   (config: AxiosRequestConfig): AxiosPromise;
//   (url: string, config?: AxiosRequestConfig): AxiosPromise
// }
var company = {
  name: '',
  NIP: ''
};
var orders = [] || false || false;
var offers = [] || false;
var apiInstance = function apiInstance(url, config) {
  // this.defaults = {} as AxiosRequestConfig;
  // this.interceptors = {
  //   request: {} as AxiosInterceptorManager<AxiosRequestConfig>;
  //   response: {} as AxiosInterceptorManager<AxiosResponse<any>>;
  // };
  // this.getUri = () => {return ''};
  // this.get = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.patch = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.delete = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.post = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.put = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.request = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.head = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.options = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  return {};
};
apiInstance.defaults = {};
apiInstance.interceptors = {
  request: {},
  response: {}
};

apiInstance.getUri = function () {
  return '';
};

apiInstance.request = function (req) {
  return new Promise(function () {});
};

apiInstance.get = function (url, config) {
  return new Promise(function (res2, rej) {
    console.warn('GET', url); // debugger

    var res = function res(r) {
      console.info('GET url', url, r);
      res2({
        data: {
          data: {
            results: r
          }
        }
      });
    };

    if (url.includes('/me')) {
      return res2({
        data: {
          data: company
        }
      });
    }

    if (url.includes('/job-orders/') && url.includes('/offers')) {
      var id = parseInt(url.match(/\d+/)[0]);
      console.log('/job-orders/:id/offers', id, offers); // debugger

      return res(offers);
    }

    if (url.includes('/job-orders/')) {
      var _id = parseInt(url.match(/\d+/)[0]);

      return res2({
        data: {
          data: orders[_id]
        }
      });
    }

    if (url.includes('/job-orders')) {
      console.log('orders', orders);
      return res(orders);
    } // debugger


    if (url.includes('/job-offers')) {
      return res(offers);
    }

    if (url.includes('/categories/prod')) {
      return res2({
        data: {
          data: [{
            label: 'Fotowoltaika',
            name: 'Fotowoltaika',
            value: 'Fotowoltaika'
          }, {
            label: 'Pompy ciepła',
            name: 'Pompy ciepła',
            value: 'Pompy ciepła'
          }]
        }
      });
    }

    if (url.includes('/categories/job')) {
      // debugger
      return res2({
        data: {
          data: [{
            label: 'Montaż',
            name: 'Montaż',
            value: 'Montaż'
          }, {
            label: 'Projekt',
            name: 'Projekt',
            value: 'Projekt'
          }]
        }
      });
    }

    rej('err');
  });
};

apiInstance.patch = function (url, data, config) {
  return new Promise(function (res) {
    company = _objectSpread(_objectSpread({}, company), data); // debugger

    console.info('saved companies/me', company);
    return res(company);
  });
};

apiInstance["delete"] = function (url, config) {
  return new Promise(function () {});
};

apiInstance.post = function (url, data, config) {
  // return new Promise(()=>{})
  return new Promise(function (res2, rej) {
    console.log('POST', url, data);
    debugger;

    var res = function res(r) {
      return res2({
        data: {
          data: {
            results: r
          }
        }
      });
    };

    if (url.includes('/offers')) {
      var jobId = parseInt(url.match(/\d+/)[0]);
      data = _objectSpread(_objectSpread({
        id: offers.length || 0
      }, data), {}, {
        jobOrderId: jobId
      }); // offers.push(data)
      // return res(offers.slice(-1)[0])

      offers.splice(0, 0, data);
      return res(offers.slice(0)[0]);
    }

    if (url.includes('/job-orders')) {
      // debugger
      data = _objectSpread(_objectSpread({
        id: orders.length || 0,
        creator: {
          company: company
        }
      }, data), {}, {
        acceptingOffersUntil: new Date(data.acceptingOffersUntil).toISOString(),
        executionUntil: new Date(data.executionUntil).toISOString()
      });
      console.log('POST job-order', data); // orders.push(data)
      // return res(orders.slice(-1)[0])

      orders.splice(0, 0, data);
      return res(orders[0]);
    }

    rej('err');
  });
};

apiInstance.put = function (url, config) {
  return new Promise(function () {});
};

apiInstance.head = function (url, config) {
  return new Promise(function () {});
};

apiInstance.options = function (url, config) {
  return new Promise(function () {});
};
/*


export class API2 implements AxiosInstance {
  // constructor(url: string | AxiosRequestConfig, config?: AxiosRequestConfig) {
  // }
  constructor(config: AxiosRequestConfig): AxiosPromise<any> {
  }
  defaults: {};
  // interceptors: {request: {use(){return 0}, eject(){}}, response: {use(){return 0}, eject(){}}};
  interceptors: {
    request: AxiosInterceptorManager<AxiosRequestConfig>;
    response: AxiosInterceptorManager<AxiosResponse<any>>;
  };
  // getUri(){return ''};
  // get(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // patch(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // delete(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // post(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // put(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // request(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // head(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // options(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  getUri(config?: AxiosRequestConfig): string {return ''};
  request<T = any, R = AxiosResponse<T>> (config: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  get<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  delete<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  head<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  options<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  post<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  put<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  patch<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
}

// export const apiInstance: AxiosInstance = function (url: string | AxiosRequestConfig, config?: AxiosRequestConfig): AxiosPromise {
// }

*/

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "../../node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vbGliL21vY2theGlvcy50cyJdLCJuYW1lcyI6WyJjb21wYW55IiwibmFtZSIsIk5JUCIsIm9yZGVycyIsIm9mZmVycyIsImFwaUluc3RhbmNlIiwidXJsIiwiY29uZmlnIiwiZGVmYXVsdHMiLCJpbnRlcmNlcHRvcnMiLCJyZXF1ZXN0IiwicmVzcG9uc2UiLCJnZXRVcmkiLCJyZXEiLCJQcm9taXNlIiwiZ2V0IiwicmVzMiIsInJlaiIsImNvbnNvbGUiLCJ3YXJuIiwicmVzIiwiciIsImluZm8iLCJkYXRhIiwicmVzdWx0cyIsImluY2x1ZGVzIiwiaWQiLCJwYXJzZUludCIsIm1hdGNoIiwibG9nIiwibGFiZWwiLCJ2YWx1ZSIsInBhdGNoIiwicG9zdCIsImpvYklkIiwibGVuZ3RoIiwiam9iT3JkZXJJZCIsInNwbGljZSIsInNsaWNlIiwiY3JlYXRvciIsImFjY2VwdGluZ09mZmVyc1VudGlsIiwiRGF0ZSIsInRvSVNPU3RyaW5nIiwiZXhlY3V0aW9uVW50aWwiLCJwdXQiLCJoZWFkIiwib3B0aW9ucyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsSUFBSUEsT0FBTyxHQUFHO0FBQ1pDLE1BQUksRUFBRSxFQURNO0FBRVpDLEtBQUcsRUFBRTtBQUZPLENBQWQ7QUFJQSxJQUFJQyxNQUFNLEdBQUcsTUFBTSxLQUFOLElBQW1CLEtBQWhDO0FBS0EsSUFBSUMsTUFBTSxHQUFHLE1BQU0sS0FBbkI7QUFNTyxJQUFNQyxXQUEwQixHQUFHLFNBQTdCQSxXQUE2QixDQUFVQyxHQUFWLEVBQTRDQyxNQUE1QyxFQUF5RTtBQUNqSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsU0FBTyxFQUFQO0FBQ0QsQ0FsQk07QUFtQlBGLFdBQVcsQ0FBQ0csUUFBWixHQUF1QixFQUF2QjtBQUNBSCxXQUFXLENBQUNJLFlBQVosR0FBMkI7QUFDekJDLFNBQU8sRUFBRSxFQURnQjtBQUV6QkMsVUFBUSxFQUFFO0FBRmUsQ0FBM0I7O0FBSUFOLFdBQVcsQ0FBQ08sTUFBWixHQUFxQixZQUFNO0FBQUMsU0FBTyxFQUFQO0FBQVUsQ0FBdEM7O0FBQ0FQLFdBQVcsQ0FBQ0ssT0FBWixHQUFzQixVQUFDRyxHQUFELEVBQTZCO0FBQUMsU0FBTyxJQUFJQyxPQUFKLENBQVksWUFBSSxDQUFFLENBQWxCLENBQVA7QUFBMkIsQ0FBL0U7O0FBQ0FULFdBQVcsQ0FBQ1UsR0FBWixHQUFrQixVQUFDVCxHQUFELEVBQWNDLE1BQWQsRUFBOEM7QUFDOUQsU0FBTyxJQUFJTyxPQUFKLENBQVksVUFBQ0UsSUFBRCxFQUFPQyxHQUFQLEVBQWU7QUFDaENDLFdBQU8sQ0FBQ0MsSUFBUixDQUFhLEtBQWIsRUFBb0JiLEdBQXBCLEVBRGdDLENBRWhDOztBQUNBLFFBQU1jLEdBQUcsR0FBRyxTQUFOQSxHQUFNLENBQUNDLENBQUQsRUFBTztBQUNqQkgsYUFBTyxDQUFDSSxJQUFSLENBQWEsU0FBYixFQUF3QmhCLEdBQXhCLEVBQTZCZSxDQUE3QjtBQUNBTCxVQUFJLENBQUM7QUFBQ08sWUFBSSxFQUFFO0FBQUNBLGNBQUksRUFBRTtBQUFDQyxtQkFBTyxFQUFFSDtBQUFWO0FBQVA7QUFBUCxPQUFELENBQUo7QUFDRCxLQUhEOztBQUlBLFFBQUlmLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxLQUFiLENBQUosRUFBeUI7QUFDdkIsYUFBT1QsSUFBSSxDQUFDO0FBQUNPLFlBQUksRUFBRTtBQUFDQSxjQUFJLEVBQUV2QjtBQUFQO0FBQVAsT0FBRCxDQUFYO0FBQ0Q7O0FBQ0QsUUFBSU0sR0FBRyxDQUFDbUIsUUFBSixDQUFhLGNBQWIsS0FBZ0NuQixHQUFHLENBQUNtQixRQUFKLENBQWEsU0FBYixDQUFwQyxFQUE2RDtBQUMzRCxVQUFNQyxFQUFFLEdBQUdDLFFBQVEsQ0FBQ3JCLEdBQUcsQ0FBQ3NCLEtBQUosQ0FBVSxLQUFWLEVBQWlCLENBQWpCLENBQUQsQ0FBbkI7QUFDQVYsYUFBTyxDQUFDVyxHQUFSLENBQVksd0JBQVosRUFBc0NILEVBQXRDLEVBQTBDdEIsTUFBMUMsRUFGMkQsQ0FHM0Q7O0FBQ0EsYUFBT2dCLEdBQUcsQ0FBQ2hCLE1BQUQsQ0FBVjtBQUNEOztBQUNELFFBQUlFLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxjQUFiLENBQUosRUFBa0M7QUFDaEMsVUFBTUMsR0FBRSxHQUFHQyxRQUFRLENBQUNyQixHQUFHLENBQUNzQixLQUFKLENBQVUsS0FBVixFQUFpQixDQUFqQixDQUFELENBQW5COztBQUNBLGFBQU9aLElBQUksQ0FBQztBQUFDTyxZQUFJLEVBQUU7QUFBQ0EsY0FBSSxFQUFFcEIsTUFBTSxDQUFDdUIsR0FBRDtBQUFiO0FBQVAsT0FBRCxDQUFYO0FBQ0Q7O0FBQ0QsUUFBSXBCLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxhQUFiLENBQUosRUFBaUM7QUFDL0JQLGFBQU8sQ0FBQ1csR0FBUixDQUFZLFFBQVosRUFBc0IxQixNQUF0QjtBQUNBLGFBQU9pQixHQUFHLENBQUNqQixNQUFELENBQVY7QUFDRCxLQXZCK0IsQ0F3QmhDOzs7QUFDQSxRQUFJRyxHQUFHLENBQUNtQixRQUFKLENBQWEsYUFBYixDQUFKLEVBQWlDO0FBQy9CLGFBQU9MLEdBQUcsQ0FBQ2hCLE1BQUQsQ0FBVjtBQUNEOztBQUNELFFBQUlFLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxrQkFBYixDQUFKLEVBQXNDO0FBQ3BDLGFBQU9ULElBQUksQ0FBQztBQUFDTyxZQUFJLEVBQUU7QUFBQ0EsY0FBSSxFQUN0QixDQUFDO0FBQUNPLGlCQUFLLEVBQUUsY0FBUjtBQUF3QjdCLGdCQUFJLEVBQUUsY0FBOUI7QUFBOEM4QixpQkFBSyxFQUFFO0FBQXJELFdBQUQsRUFBc0U7QUFBQ0QsaUJBQUssRUFBRSxjQUFSO0FBQXdCN0IsZ0JBQUksRUFBRSxjQUE5QjtBQUE4QzhCLGlCQUFLLEVBQUU7QUFBckQsV0FBdEU7QUFEaUI7QUFBUCxPQUFELENBQVg7QUFHRDs7QUFDRCxRQUFJekIsR0FBRyxDQUFDbUIsUUFBSixDQUFhLGlCQUFiLENBQUosRUFBcUM7QUFDbkM7QUFDQSxhQUFPVCxJQUFJLENBQUM7QUFBQ08sWUFBSSxFQUFFO0FBQUNBLGNBQUksRUFDckIsQ0FBQztBQUFDTyxpQkFBSyxFQUFFLFFBQVI7QUFBa0I3QixnQkFBSSxFQUFFLFFBQXhCO0FBQWtDOEIsaUJBQUssRUFBRTtBQUF6QyxXQUFELEVBQW9EO0FBQUNELGlCQUFLLEVBQUUsU0FBUjtBQUFtQjdCLGdCQUFJLEVBQUUsU0FBekI7QUFBb0M4QixpQkFBSyxFQUFFO0FBQTNDLFdBQXBEO0FBRGdCO0FBQVAsT0FBRCxDQUFYO0FBR0Q7O0FBQ0RkLE9BQUcsQ0FBQyxLQUFELENBQUg7QUFDRCxHQXhDTSxDQUFQO0FBeUNELENBMUNEOztBQTJDQVosV0FBVyxDQUFDMkIsS0FBWixHQUFvQixVQUFDMUIsR0FBRCxFQUFjaUIsSUFBZCxFQUF5QmhCLE1BQXpCLEVBQXlEO0FBQUMsU0FBTyxJQUFJTyxPQUFKLENBQWlCLFVBQUNNLEdBQUQsRUFBTztBQUMzR3BCLFdBQU8sbUNBQU9BLE9BQVAsR0FBbUJ1QixJQUFuQixDQUFQLENBRDJHLENBRTNHOztBQUNBTCxXQUFPLENBQUNJLElBQVIsQ0FBYSxvQkFBYixFQUFtQ3RCLE9BQW5DO0FBQ0EsV0FBT29CLEdBQUcsQ0FBQ3BCLE9BQUQsQ0FBVjtBQUNELEdBTG9GLENBQVA7QUFLM0UsQ0FMSDs7QUFNQUssV0FBVyxVQUFYLEdBQXFCLFVBQUNDLEdBQUQsRUFBY0MsTUFBZCxFQUE4QztBQUFDLFNBQU8sSUFBSU8sT0FBSixDQUFZLFlBQUksQ0FBRSxDQUFsQixDQUFQO0FBQTJCLENBQS9GOztBQUNBVCxXQUFXLENBQUM0QixJQUFaLEdBQW1CLFVBQUMzQixHQUFELEVBQWNpQixJQUFkLEVBQXlCaEIsTUFBekIsRUFBeUQ7QUFDMUU7QUFDQSxTQUFPLElBQUlPLE9BQUosQ0FBWSxVQUFDRSxJQUFELEVBQU9DLEdBQVAsRUFBZTtBQUNoQ0MsV0FBTyxDQUFDVyxHQUFSLENBQVksTUFBWixFQUFvQnZCLEdBQXBCLEVBQXlCaUIsSUFBekI7QUFDQTs7QUFDQSxRQUFNSCxHQUFHLEdBQUcsU0FBTkEsR0FBTSxDQUFDQyxDQUFEO0FBQUEsYUFBT0wsSUFBSSxDQUFDO0FBQUNPLFlBQUksRUFBRTtBQUFDQSxjQUFJLEVBQUU7QUFBQ0MsbUJBQU8sRUFBRUg7QUFBVjtBQUFQO0FBQVAsT0FBRCxDQUFYO0FBQUEsS0FBWjs7QUFDQSxRQUFJZixHQUFHLENBQUNtQixRQUFKLENBQWEsU0FBYixDQUFKLEVBQTZCO0FBQzNCLFVBQU1TLEtBQUssR0FBR1AsUUFBUSxDQUFDckIsR0FBRyxDQUFDc0IsS0FBSixDQUFVLEtBQVYsRUFBaUIsQ0FBakIsQ0FBRCxDQUF0QjtBQUNBTCxVQUFJO0FBQ0ZHLFVBQUUsRUFBR3RCLE1BQU0sQ0FBQytCLE1BQVAsSUFBaUI7QUFEcEIsU0FFQ1osSUFGRDtBQUdGYSxrQkFBVSxFQUFFRjtBQUhWLFFBQUosQ0FGMkIsQ0FPM0I7QUFDQTs7QUFDQTlCLFlBQU0sQ0FBQ2lDLE1BQVAsQ0FBYyxDQUFkLEVBQWlCLENBQWpCLEVBQW9CZCxJQUFwQjtBQUNBLGFBQU9ILEdBQUcsQ0FBQ2hCLE1BQU0sQ0FBQ2tDLEtBQVAsQ0FBYSxDQUFiLEVBQWdCLENBQWhCLENBQUQsQ0FBVjtBQUNEOztBQUNELFFBQUloQyxHQUFHLENBQUNtQixRQUFKLENBQWEsYUFBYixDQUFKLEVBQWlDO0FBQy9CO0FBQ0FGLFVBQUk7QUFDRkcsVUFBRSxFQUFHdkIsTUFBTSxDQUFDZ0MsTUFBUCxJQUFpQixDQURwQjtBQUVGSSxlQUFPLEVBQUU7QUFDUHZDLGlCQUFPLEVBQVBBO0FBRE87QUFGUCxTQUtDdUIsSUFMRDtBQU1GaUIsNEJBQW9CLEVBQUUsSUFBSUMsSUFBSixDQUFTbEIsSUFBSSxDQUFDaUIsb0JBQWQsRUFBb0NFLFdBQXBDLEVBTnBCO0FBT0ZDLHNCQUFjLEVBQUUsSUFBSUYsSUFBSixDQUFTbEIsSUFBSSxDQUFDb0IsY0FBZCxFQUE4QkQsV0FBOUI7QUFQZCxRQUFKO0FBU0F4QixhQUFPLENBQUNXLEdBQVIsQ0FBWSxnQkFBWixFQUE4Qk4sSUFBOUIsRUFYK0IsQ0FZL0I7QUFDQTs7QUFDQXBCLFlBQU0sQ0FBQ2tDLE1BQVAsQ0FBYyxDQUFkLEVBQWlCLENBQWpCLEVBQW9CZCxJQUFwQjtBQUNBLGFBQU9ILEdBQUcsQ0FBQ2pCLE1BQU0sQ0FBQyxDQUFELENBQVAsQ0FBVjtBQUNEOztBQUNEYyxPQUFHLENBQUMsS0FBRCxDQUFIO0FBQ0QsR0FsQ00sQ0FBUDtBQW1DRCxDQXJDRDs7QUFzQ0FaLFdBQVcsQ0FBQ3VDLEdBQVosR0FBa0IsVUFBQ3RDLEdBQUQsRUFBY0MsTUFBZCxFQUE4QztBQUFDLFNBQU8sSUFBSU8sT0FBSixDQUFZLFlBQUksQ0FBRSxDQUFsQixDQUFQO0FBQTJCLENBQTVGOztBQUNBVCxXQUFXLENBQUN3QyxJQUFaLEdBQW1CLFVBQUN2QyxHQUFELEVBQWNDLE1BQWQsRUFBOEM7QUFBQyxTQUFPLElBQUlPLE9BQUosQ0FBWSxZQUFJLENBQUUsQ0FBbEIsQ0FBUDtBQUEyQixDQUE3Rjs7QUFDQVQsV0FBVyxDQUFDeUMsT0FBWixHQUFzQixVQUFDeEMsR0FBRCxFQUFjQyxNQUFkLEVBQThDO0FBQUMsU0FBTyxJQUFJTyxPQUFKLENBQVksWUFBSSxDQUFFLENBQWxCLENBQVA7QUFBMkIsQ0FBaEc7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvam9iL1tqb2JJZF0vb2ZmZXJzLmFhYmRmOWM1ODJkNTg2ZDg2M2I3LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBeGlvc0luc3RhbmNlLCBBeGlvc0ludGVyY2VwdG9yTWFuYWdlciwgQXhpb3NQcm9taXNlLCBBeGlvc1JlcXVlc3RDb25maWcsIEF4aW9zUmVzcG9uc2UgfSBmcm9tIFwiYXhpb3NcIjtcclxuXHJcbi8vIGV4cG9ydCBpbnRlcmZhY2UgQXhpb3NJbnN0YW5jZTIge1xyXG4vLyAgIChjb25maWc6IEF4aW9zUmVxdWVzdENvbmZpZyk6IEF4aW9zUHJvbWlzZTtcclxuLy8gICAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZyk6IEF4aW9zUHJvbWlzZVxyXG4vLyB9XHJcblxyXG5sZXQgY29tcGFueSA9IHtcclxuICBuYW1lOiAnJyxcclxuICBOSVA6ICcnLFxyXG59XHJcbmxldCBvcmRlcnMgPSBbXSB8fCBbe2lkOiAwfV0gfHwgW3tcclxuICBjcmVhdG9yOiB7XHJcbiAgICBjb21wYW55OiB7Li4uY29tcGFueX0sXHJcbiAgfSxcclxufV1cclxubGV0IG9mZmVycyA9IFtdIHx8IFt7XHJcbiAgaWQ6IDAsXHJcbiAgam9iT2ZmZXJJZDogMCxcclxuICBwcmljZU5ldDogMTUwMCxcclxufV1cclxuXHJcbmV4cG9ydCBjb25zdCBhcGlJbnN0YW5jZTogQXhpb3NJbnN0YW5jZSA9IGZ1bmN0aW9uICh1cmw6IHN0cmluZyB8IEF4aW9zUmVxdWVzdENvbmZpZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSB7XHJcbiAgLy8gdGhpcy5kZWZhdWx0cyA9IHt9IGFzIEF4aW9zUmVxdWVzdENvbmZpZztcclxuICAvLyB0aGlzLmludGVyY2VwdG9ycyA9IHtcclxuICAvLyAgIHJlcXVlc3Q6IHt9IGFzIEF4aW9zSW50ZXJjZXB0b3JNYW5hZ2VyPEF4aW9zUmVxdWVzdENvbmZpZz47XHJcbiAgLy8gICByZXNwb25zZToge30gYXMgQXhpb3NJbnRlcmNlcHRvck1hbmFnZXI8QXhpb3NSZXNwb25zZTxhbnk+PjtcclxuICAvLyB9O1xyXG4gIFxyXG4gIC8vIHRoaXMuZ2V0VXJpID0gKCkgPT4ge3JldHVybiAnJ307XHJcbiAgLy8gdGhpcy5nZXQgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyB0aGlzLnBhdGNoID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gdGhpcy5kZWxldGUgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyB0aGlzLnBvc3QgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyB0aGlzLnB1dCA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHRoaXMucmVxdWVzdCA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHRoaXMuaGVhZCA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHRoaXMub3B0aW9ucyA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG5cclxuICByZXR1cm4ge30gYXMgQXhpb3NQcm9taXNlPGFueT47XHJcbn1cclxuYXBpSW5zdGFuY2UuZGVmYXVsdHMgPSB7fSBhcyBBeGlvc1JlcXVlc3RDb25maWdcclxuYXBpSW5zdGFuY2UuaW50ZXJjZXB0b3JzID0ge1xyXG4gIHJlcXVlc3Q6IHt9IGFzIEF4aW9zSW50ZXJjZXB0b3JNYW5hZ2VyPEF4aW9zUmVxdWVzdENvbmZpZz4sXHJcbiAgcmVzcG9uc2U6IHt9IGFzIEF4aW9zSW50ZXJjZXB0b3JNYW5hZ2VyPEF4aW9zUmVzcG9uc2U8YW55Pj5cclxufVxyXG5hcGlJbnN0YW5jZS5nZXRVcmkgPSAoKSA9PiB7cmV0dXJuICcnfTtcclxuYXBpSW5zdGFuY2UucmVxdWVzdCA9IChyZXE6IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuYXBpSW5zdGFuY2UuZ2V0ID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtcclxuICByZXR1cm4gbmV3IFByb21pc2UoKHJlczIsIHJlaikgPT4ge1xyXG4gICAgY29uc29sZS53YXJuKCdHRVQnLCB1cmwpXHJcbiAgICAvLyBkZWJ1Z2dlclxyXG4gICAgY29uc3QgcmVzID0gKHIpID0+IHtcclxuICAgICAgY29uc29sZS5pbmZvKCdHRVQgdXJsJywgdXJsLCByKVxyXG4gICAgICByZXMyKHtkYXRhOiB7ZGF0YToge3Jlc3VsdHM6IHJ9fX0pXHJcbiAgICB9XHJcbiAgICBpZiAodXJsLmluY2x1ZGVzKCcvbWUnKSkge1xyXG4gICAgICByZXR1cm4gcmVzMih7ZGF0YToge2RhdGE6IGNvbXBhbnl9fSlcclxuICAgIH1cclxuICAgIGlmICh1cmwuaW5jbHVkZXMoJy9qb2Itb3JkZXJzLycpICYmIHVybC5pbmNsdWRlcygnL29mZmVycycpKSB7XHJcbiAgICAgIGNvbnN0IGlkID0gcGFyc2VJbnQodXJsLm1hdGNoKC9cXGQrLylbMF0pXHJcbiAgICAgIGNvbnNvbGUubG9nKCcvam9iLW9yZGVycy86aWQvb2ZmZXJzJywgaWQsIG9mZmVycylcclxuICAgICAgLy8gZGVidWdnZXJcclxuICAgICAgcmV0dXJuIHJlcyhvZmZlcnMpXHJcbiAgICB9XHJcbiAgICBpZiAodXJsLmluY2x1ZGVzKCcvam9iLW9yZGVycy8nKSkge1xyXG4gICAgICBjb25zdCBpZCA9IHBhcnNlSW50KHVybC5tYXRjaCgvXFxkKy8pWzBdKVxyXG4gICAgICByZXR1cm4gcmVzMih7ZGF0YToge2RhdGE6IG9yZGVyc1tpZF19fSlcclxuICAgIH1cclxuICAgIGlmICh1cmwuaW5jbHVkZXMoJy9qb2Itb3JkZXJzJykpIHtcclxuICAgICAgY29uc29sZS5sb2coJ29yZGVycycsIG9yZGVycylcclxuICAgICAgcmV0dXJuIHJlcyhvcmRlcnMpXHJcbiAgICB9XHJcbiAgICAvLyBkZWJ1Z2dlclxyXG4gICAgaWYgKHVybC5pbmNsdWRlcygnL2pvYi1vZmZlcnMnKSkge1xyXG4gICAgICByZXR1cm4gcmVzKG9mZmVycylcclxuICAgIH1cclxuICAgIGlmICh1cmwuaW5jbHVkZXMoJy9jYXRlZ29yaWVzL3Byb2QnKSkge1xyXG4gICAgICByZXR1cm4gcmVzMih7ZGF0YToge2RhdGE6IFxyXG4gICAgICAgIFt7bGFiZWw6ICdGb3Rvd29sdGFpa2EnLCBuYW1lOiAnRm90b3dvbHRhaWthJywgdmFsdWU6ICdGb3Rvd29sdGFpa2EnfSx7bGFiZWw6ICdQb21weSBjaWVwxYJhJywgbmFtZTogJ1BvbXB5IGNpZXDFgmEnLCB2YWx1ZTogJ1BvbXB5IGNpZXDFgmEnfV1cclxuICAgICAgfX0pXHJcbiAgICB9XHJcbiAgICBpZiAodXJsLmluY2x1ZGVzKCcvY2F0ZWdvcmllcy9qb2InKSkge1xyXG4gICAgICAvLyBkZWJ1Z2dlclxyXG4gICAgICByZXR1cm4gcmVzMih7ZGF0YToge2RhdGE6IFxyXG4gICAgICAgIChbe2xhYmVsOiAnTW9udGHFvCcsIG5hbWU6ICdNb250YcW8JywgdmFsdWU6ICdNb250YcW8J30se2xhYmVsOiAnUHJvamVrdCcsIG5hbWU6ICdQcm9qZWt0JywgdmFsdWU6ICdQcm9qZWt0J31dKVxyXG4gICAgICB9fSlcclxuICAgIH1cclxuICAgIHJlaignZXJyJylcclxuICB9KSBhcyB1bmtub3duIGFzIFByb21pc2U8YW55PlxyXG59O1xyXG5hcGlJbnN0YW5jZS5wYXRjaCA9ICh1cmw6IHN0cmluZywgZGF0YTogYW55LCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2U8YW55PigocmVzKT0+e1xyXG4gIGNvbXBhbnkgPSB7Li4uY29tcGFueSwgLi4uZGF0YX1cclxuICAvLyBkZWJ1Z2dlclxyXG4gIGNvbnNvbGUuaW5mbygnc2F2ZWQgY29tcGFuaWVzL21lJywgY29tcGFueSlcclxuICByZXR1cm4gcmVzKGNvbXBhbnkpXHJcbn0pfTtcclxuYXBpSW5zdGFuY2UuZGVsZXRlID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbmFwaUluc3RhbmNlLnBvc3QgPSAodXJsOiBzdHJpbmcsIGRhdGE6IGFueSwgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7XHJcbiAgLy8gcmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSlcclxuICByZXR1cm4gbmV3IFByb21pc2UoKHJlczIsIHJlaikgPT4ge1xyXG4gICAgY29uc29sZS5sb2coJ1BPU1QnLCB1cmwsIGRhdGEpXHJcbiAgICBkZWJ1Z2dlclxyXG4gICAgY29uc3QgcmVzID0gKHIpID0+IHJlczIoe2RhdGE6IHtkYXRhOiB7cmVzdWx0czogcn19fSlcclxuICAgIGlmICh1cmwuaW5jbHVkZXMoJy9vZmZlcnMnKSkge1xyXG4gICAgICBjb25zdCBqb2JJZCA9IHBhcnNlSW50KHVybC5tYXRjaCgvXFxkKy8pWzBdKVxyXG4gICAgICBkYXRhID0ge1xyXG4gICAgICAgIGlkOiAob2ZmZXJzLmxlbmd0aCB8fCAwKSxcclxuICAgICAgICAuLi5kYXRhLFxyXG4gICAgICAgIGpvYk9yZGVySWQ6IGpvYklkLFxyXG4gICAgICB9XHJcbiAgICAgIC8vIG9mZmVycy5wdXNoKGRhdGEpXHJcbiAgICAgIC8vIHJldHVybiByZXMob2ZmZXJzLnNsaWNlKC0xKVswXSlcclxuICAgICAgb2ZmZXJzLnNwbGljZSgwLCAwLCBkYXRhKVxyXG4gICAgICByZXR1cm4gcmVzKG9mZmVycy5zbGljZSgwKVswXSlcclxuICAgIH1cclxuICAgIGlmICh1cmwuaW5jbHVkZXMoJy9qb2Itb3JkZXJzJykpIHtcclxuICAgICAgLy8gZGVidWdnZXJcclxuICAgICAgZGF0YSA9IHtcclxuICAgICAgICBpZDogKG9yZGVycy5sZW5ndGggfHwgMCksXHJcbiAgICAgICAgY3JlYXRvcjoge1xyXG4gICAgICAgICAgY29tcGFueSxcclxuICAgICAgICB9LFxyXG4gICAgICAgIC4uLmRhdGEsXHJcbiAgICAgICAgYWNjZXB0aW5nT2ZmZXJzVW50aWw6IG5ldyBEYXRlKGRhdGEuYWNjZXB0aW5nT2ZmZXJzVW50aWwpLnRvSVNPU3RyaW5nKCksXHJcbiAgICAgICAgZXhlY3V0aW9uVW50aWw6IG5ldyBEYXRlKGRhdGEuZXhlY3V0aW9uVW50aWwpLnRvSVNPU3RyaW5nKCksXHJcbiAgICAgIH1cclxuICAgICAgY29uc29sZS5sb2coJ1BPU1Qgam9iLW9yZGVyJywgZGF0YSlcclxuICAgICAgLy8gb3JkZXJzLnB1c2goZGF0YSlcclxuICAgICAgLy8gcmV0dXJuIHJlcyhvcmRlcnMuc2xpY2UoLTEpWzBdKVxyXG4gICAgICBvcmRlcnMuc3BsaWNlKDAsIDAsIGRhdGEpXHJcbiAgICAgIHJldHVybiByZXMob3JkZXJzWzBdKVxyXG4gICAgfVxyXG4gICAgcmVqKCdlcnInKVxyXG4gIH0pIGFzIHVua25vd24gYXMgUHJvbWlzZTxhbnk+XHJcbn07XHJcbmFwaUluc3RhbmNlLnB1dCA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG5hcGlJbnN0YW5jZS5oZWFkID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbmFwaUluc3RhbmNlLm9wdGlvbnMgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuXHJcbi8qXHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIEFQSTIgaW1wbGVtZW50cyBBeGlvc0luc3RhbmNlIHtcclxuICAvLyBjb25zdHJ1Y3Rvcih1cmw6IHN0cmluZyB8IEF4aW9zUmVxdWVzdENvbmZpZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSB7XHJcbiAgLy8gfVxyXG4gIGNvbnN0cnVjdG9yKGNvbmZpZzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogQXhpb3NQcm9taXNlPGFueT4ge1xyXG4gIH1cclxuICBkZWZhdWx0czoge307XHJcbiAgLy8gaW50ZXJjZXB0b3JzOiB7cmVxdWVzdDoge3VzZSgpe3JldHVybiAwfSwgZWplY3QoKXt9fSwgcmVzcG9uc2U6IHt1c2UoKXtyZXR1cm4gMH0sIGVqZWN0KCl7fX19O1xyXG4gIGludGVyY2VwdG9yczoge1xyXG4gICAgcmVxdWVzdDogQXhpb3NJbnRlcmNlcHRvck1hbmFnZXI8QXhpb3NSZXF1ZXN0Q29uZmlnPjtcclxuICAgIHJlc3BvbnNlOiBBeGlvc0ludGVyY2VwdG9yTWFuYWdlcjxBeGlvc1Jlc3BvbnNlPGFueT4+O1xyXG4gIH07XHJcbiAgLy8gZ2V0VXJpKCl7cmV0dXJuICcnfTtcclxuICAvLyBnZXQodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyBwYXRjaCh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIGRlbGV0ZSh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHBvc3QodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyBwdXQodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyByZXF1ZXN0KHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpIHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gaGVhZCh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIG9wdGlvbnModXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICBnZXRVcmkoY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogc3RyaW5nIHtyZXR1cm4gJyd9O1xyXG4gIHJlcXVlc3Q8VCA9IGFueSwgUiA9IEF4aW9zUmVzcG9uc2U8VD4+IChjb25maWc6IEF4aW9zUmVxdWVzdENvbmZpZyk6IFByb21pc2U8Uj4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSlcclxuICB9O1xyXG4gIGdldDxUID0gYW55LCBSID0gQXhpb3NSZXNwb25zZTxUPj4odXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZyk6IFByb21pc2U8Uj4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSlcclxuICB9O1xyXG4gIGRlbGV0ZTxUID0gYW55LCBSID0gQXhpb3NSZXNwb25zZTxUPj4odXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZyk6IFByb21pc2U8Uj4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSlcclxuICB9O1xyXG4gIGhlYWQ8VCA9IGFueSwgUiA9IEF4aW9zUmVzcG9uc2U8VD4+KHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpOiBQcm9taXNlPFI+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pXHJcbiAgfTtcclxuICBvcHRpb25zPFQgPSBhbnksIFIgPSBBeGlvc1Jlc3BvbnNlPFQ+Pih1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogUHJvbWlzZTxSPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KVxyXG4gIH07XHJcbiAgcG9zdDxUID0gYW55LCBSID0gQXhpb3NSZXNwb25zZTxUPj4odXJsOiBzdHJpbmcsIGRhdGE/OiBhbnksIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZyk6IFByb21pc2U8Uj4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSlcclxuICB9O1xyXG4gIHB1dDxUID0gYW55LCBSID0gQXhpb3NSZXNwb25zZTxUPj4odXJsOiBzdHJpbmcsIGRhdGE/OiBhbnksIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZyk6IFByb21pc2U8Uj4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSlcclxuICB9O1xyXG4gIHBhdGNoPFQgPSBhbnksIFIgPSBBeGlvc1Jlc3BvbnNlPFQ+Pih1cmw6IHN0cmluZywgZGF0YT86IGFueSwgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogUHJvbWlzZTxSPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KVxyXG4gIH07XHJcbn1cclxuXHJcbi8vIGV4cG9ydCBjb25zdCBhcGlJbnN0YW5jZTogQXhpb3NJbnN0YW5jZSA9IGZ1bmN0aW9uICh1cmw6IHN0cmluZyB8IEF4aW9zUmVxdWVzdENvbmZpZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogQXhpb3NQcm9taXNlIHtcclxuLy8gfVxyXG5cclxuKi8iXSwic291cmNlUm9vdCI6IiJ9