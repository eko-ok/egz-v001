webpackHotUpdate_N_E("pages/job/[jobId]",{

/***/ "./lib/mockaxios.ts":
/*!**************************!*\
  !*** ./lib/mockaxios.ts ***!
  \**************************/
/*! exports provided: apiInstance */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "apiInstance", function() { return apiInstance; });
/* harmony import */ var C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! C:/Users/azochniak/Desktop/ecop/trading-platform/node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "../../node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(C_Users_azochniak_Desktop_ecop_trading_platform_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

// export interface AxiosInstance2 {
//   (config: AxiosRequestConfig): AxiosPromise;
//   (url: string, config?: AxiosRequestConfig): AxiosPromise
// }
var company = {
  name: '',
  NIP: ''
};
var orders = [] || false || false;
var offers = [] || false;
var apiInstance = function apiInstance(url, config) {
  // this.defaults = {} as AxiosRequestConfig;
  // this.interceptors = {
  //   request: {} as AxiosInterceptorManager<AxiosRequestConfig>;
  //   response: {} as AxiosInterceptorManager<AxiosResponse<any>>;
  // };
  // this.getUri = () => {return ''};
  // this.get = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.patch = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.delete = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.post = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.put = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.request = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.head = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  // this.options = (url: string, config?: AxiosRequestConfig) => {return new Promise(()=>{})};
  return {};
};
apiInstance.defaults = {};
apiInstance.interceptors = {
  request: {},
  response: {}
};

apiInstance.getUri = function () {
  return '';
};

apiInstance.request = function (req) {
  return new Promise(function () {});
};

apiInstance.get = function (url, config) {
  return new Promise(function (res2, rej) {
    console.warn('GET', url); // debugger

    var res = function res(r) {
      console.info('GET url', url, r);
      res2({
        data: {
          data: {
            results: r
          }
        }
      });
    };

    if (url.includes('/me')) {
      return res2({
        data: {
          data: company
        }
      });
    }

    if (url.includes('/job-orders/') && url.includes('/offers')) {
      var id = parseInt(url.match(/\d+/)[0]);
      console.log('/job-orders/:id/offers', id, offers); // debugger

      return res(offers);
    }

    if (url.includes('/job-orders/')) {
      var _id = parseInt(url.match(/\d+/)[0]);

      return res2({
        data: {
          data: orders[_id]
        }
      });
    }

    if (url.includes('/job-orders')) {
      console.log('orders', orders);
      return res(orders);
    } // debugger


    if (url.includes('/job-offers')) {
      return res(offers);
    }

    if (url.includes('/categories/prod')) {
      return res2({
        data: {
          data: [{
            label: 'Fotowoltaika',
            name: 'Fotowoltaika',
            value: 'Fotowoltaika'
          }, {
            label: 'Pompy ciepła',
            name: 'Pompy ciepła',
            value: 'Pompy ciepła'
          }]
        }
      });
    }

    if (url.includes('/categories/job')) {
      // debugger
      return res2({
        data: {
          data: [{
            label: 'Montaż',
            name: 'Montaż',
            value: 'Montaż'
          }, {
            label: 'Projekt',
            name: 'Projekt',
            value: 'Projekt'
          }]
        }
      });
    }

    rej('err');
  });
};

apiInstance.patch = function (url, data, config) {
  return new Promise(function (res) {
    company = _objectSpread(_objectSpread({}, company), data); // debugger

    console.info('saved companies/me', company);
    return res(company);
  });
};

apiInstance["delete"] = function (url, config) {
  return new Promise(function () {});
};

apiInstance.post = function (url, data, config) {
  // return new Promise(()=>{})
  return new Promise(function (res2, rej) {
    console.log('POST', url, data);
    debugger;

    var res = function res(r) {
      return res2({
        data: {
          data: {
            results: r
          }
        }
      });
    };

    if (url.includes('/offers')) {
      var jobId = parseInt(url.match(/\d+/)[0]);
      data = _objectSpread(_objectSpread({
        id: offers.length || 0
      }, data), {}, {
        jobOrderId: jobId
      }); // offers.push(data)
      // return res(offers.slice(-1)[0])

      offers.splice(0, 0, data);
      return res(offers.slice(0)[0]);
    }

    if (url.includes('/job-orders')) {
      // debugger
      data = _objectSpread(_objectSpread({
        id: orders.length || 0,
        creator: {
          company: company
        }
      }, data), {}, {
        acceptingOffersUntil: new Date(data.acceptingOffersUntil).toISOString(),
        executionUntil: new Date(data.executionUntil).toISOString()
      });
      console.log('POST job-order', data); // orders.push(data)
      // return res(orders.slice(-1)[0])

      orders.splice(0, 0, data);
      return res(orders[0]);
    }

    rej('err');
  });
};

apiInstance.put = function (url, config) {
  return new Promise(function () {});
};

apiInstance.head = function (url, config) {
  return new Promise(function () {});
};

apiInstance.options = function (url, config) {
  return new Promise(function () {});
};
/*


export class API2 implements AxiosInstance {
  // constructor(url: string | AxiosRequestConfig, config?: AxiosRequestConfig) {
  // }
  constructor(config: AxiosRequestConfig): AxiosPromise<any> {
  }
  defaults: {};
  // interceptors: {request: {use(){return 0}, eject(){}}, response: {use(){return 0}, eject(){}}};
  interceptors: {
    request: AxiosInterceptorManager<AxiosRequestConfig>;
    response: AxiosInterceptorManager<AxiosResponse<any>>;
  };
  // getUri(){return ''};
  // get(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // patch(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // delete(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // post(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // put(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // request(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // head(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  // options(url: string, config?: AxiosRequestConfig) {return new Promise(()=>{})};
  getUri(config?: AxiosRequestConfig): string {return ''};
  request<T = any, R = AxiosResponse<T>> (config: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  get<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  delete<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  head<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  options<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  post<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  put<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
  patch<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return new Promise(()=>{})
  };
}

// export const apiInstance: AxiosInstance = function (url: string | AxiosRequestConfig, config?: AxiosRequestConfig): AxiosPromise {
// }

*/

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "../../node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vbGliL21vY2theGlvcy50cyJdLCJuYW1lcyI6WyJjb21wYW55IiwibmFtZSIsIk5JUCIsIm9yZGVycyIsIm9mZmVycyIsImFwaUluc3RhbmNlIiwidXJsIiwiY29uZmlnIiwiZGVmYXVsdHMiLCJpbnRlcmNlcHRvcnMiLCJyZXF1ZXN0IiwicmVzcG9uc2UiLCJnZXRVcmkiLCJyZXEiLCJQcm9taXNlIiwiZ2V0IiwicmVzMiIsInJlaiIsImNvbnNvbGUiLCJ3YXJuIiwicmVzIiwiciIsImluZm8iLCJkYXRhIiwicmVzdWx0cyIsImluY2x1ZGVzIiwiaWQiLCJwYXJzZUludCIsIm1hdGNoIiwibG9nIiwibGFiZWwiLCJ2YWx1ZSIsInBhdGNoIiwicG9zdCIsImpvYklkIiwibGVuZ3RoIiwiam9iT3JkZXJJZCIsInNwbGljZSIsInNsaWNlIiwiY3JlYXRvciIsImFjY2VwdGluZ09mZmVyc1VudGlsIiwiRGF0ZSIsInRvSVNPU3RyaW5nIiwiZXhlY3V0aW9uVW50aWwiLCJwdXQiLCJoZWFkIiwib3B0aW9ucyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsSUFBSUEsT0FBTyxHQUFHO0FBQ1pDLE1BQUksRUFBRSxFQURNO0FBRVpDLEtBQUcsRUFBRTtBQUZPLENBQWQ7QUFJQSxJQUFJQyxNQUFNLEdBQUcsTUFBTSxLQUFOLElBQW1CLEtBQWhDO0FBS0EsSUFBSUMsTUFBTSxHQUFHLE1BQU0sS0FBbkI7QUFNTyxJQUFNQyxXQUEwQixHQUFHLFNBQTdCQSxXQUE2QixDQUFVQyxHQUFWLEVBQTRDQyxNQUE1QyxFQUF5RTtBQUNqSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsU0FBTyxFQUFQO0FBQ0QsQ0FsQk07QUFtQlBGLFdBQVcsQ0FBQ0csUUFBWixHQUF1QixFQUF2QjtBQUNBSCxXQUFXLENBQUNJLFlBQVosR0FBMkI7QUFDekJDLFNBQU8sRUFBRSxFQURnQjtBQUV6QkMsVUFBUSxFQUFFO0FBRmUsQ0FBM0I7O0FBSUFOLFdBQVcsQ0FBQ08sTUFBWixHQUFxQixZQUFNO0FBQUMsU0FBTyxFQUFQO0FBQVUsQ0FBdEM7O0FBQ0FQLFdBQVcsQ0FBQ0ssT0FBWixHQUFzQixVQUFDRyxHQUFELEVBQTZCO0FBQUMsU0FBTyxJQUFJQyxPQUFKLENBQVksWUFBSSxDQUFFLENBQWxCLENBQVA7QUFBMkIsQ0FBL0U7O0FBQ0FULFdBQVcsQ0FBQ1UsR0FBWixHQUFrQixVQUFDVCxHQUFELEVBQWNDLE1BQWQsRUFBOEM7QUFDOUQsU0FBTyxJQUFJTyxPQUFKLENBQVksVUFBQ0UsSUFBRCxFQUFPQyxHQUFQLEVBQWU7QUFDaENDLFdBQU8sQ0FBQ0MsSUFBUixDQUFhLEtBQWIsRUFBb0JiLEdBQXBCLEVBRGdDLENBRWhDOztBQUNBLFFBQU1jLEdBQUcsR0FBRyxTQUFOQSxHQUFNLENBQUNDLENBQUQsRUFBTztBQUNqQkgsYUFBTyxDQUFDSSxJQUFSLENBQWEsU0FBYixFQUF3QmhCLEdBQXhCLEVBQTZCZSxDQUE3QjtBQUNBTCxVQUFJLENBQUM7QUFBQ08sWUFBSSxFQUFFO0FBQUNBLGNBQUksRUFBRTtBQUFDQyxtQkFBTyxFQUFFSDtBQUFWO0FBQVA7QUFBUCxPQUFELENBQUo7QUFDRCxLQUhEOztBQUlBLFFBQUlmLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxLQUFiLENBQUosRUFBeUI7QUFDdkIsYUFBT1QsSUFBSSxDQUFDO0FBQUNPLFlBQUksRUFBRTtBQUFDQSxjQUFJLEVBQUV2QjtBQUFQO0FBQVAsT0FBRCxDQUFYO0FBQ0Q7O0FBQ0QsUUFBSU0sR0FBRyxDQUFDbUIsUUFBSixDQUFhLGNBQWIsS0FBZ0NuQixHQUFHLENBQUNtQixRQUFKLENBQWEsU0FBYixDQUFwQyxFQUE2RDtBQUMzRCxVQUFNQyxFQUFFLEdBQUdDLFFBQVEsQ0FBQ3JCLEdBQUcsQ0FBQ3NCLEtBQUosQ0FBVSxLQUFWLEVBQWlCLENBQWpCLENBQUQsQ0FBbkI7QUFDQVYsYUFBTyxDQUFDVyxHQUFSLENBQVksd0JBQVosRUFBc0NILEVBQXRDLEVBQTBDdEIsTUFBMUMsRUFGMkQsQ0FHM0Q7O0FBQ0EsYUFBT2dCLEdBQUcsQ0FBQ2hCLE1BQUQsQ0FBVjtBQUNEOztBQUNELFFBQUlFLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxjQUFiLENBQUosRUFBa0M7QUFDaEMsVUFBTUMsR0FBRSxHQUFHQyxRQUFRLENBQUNyQixHQUFHLENBQUNzQixLQUFKLENBQVUsS0FBVixFQUFpQixDQUFqQixDQUFELENBQW5COztBQUNBLGFBQU9aLElBQUksQ0FBQztBQUFDTyxZQUFJLEVBQUU7QUFBQ0EsY0FBSSxFQUFFcEIsTUFBTSxDQUFDdUIsR0FBRDtBQUFiO0FBQVAsT0FBRCxDQUFYO0FBQ0Q7O0FBQ0QsUUFBSXBCLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxhQUFiLENBQUosRUFBaUM7QUFDL0JQLGFBQU8sQ0FBQ1csR0FBUixDQUFZLFFBQVosRUFBc0IxQixNQUF0QjtBQUNBLGFBQU9pQixHQUFHLENBQUNqQixNQUFELENBQVY7QUFDRCxLQXZCK0IsQ0F3QmhDOzs7QUFDQSxRQUFJRyxHQUFHLENBQUNtQixRQUFKLENBQWEsYUFBYixDQUFKLEVBQWlDO0FBQy9CLGFBQU9MLEdBQUcsQ0FBQ2hCLE1BQUQsQ0FBVjtBQUNEOztBQUNELFFBQUlFLEdBQUcsQ0FBQ21CLFFBQUosQ0FBYSxrQkFBYixDQUFKLEVBQXNDO0FBQ3BDLGFBQU9ULElBQUksQ0FBQztBQUFDTyxZQUFJLEVBQUU7QUFBQ0EsY0FBSSxFQUN0QixDQUFDO0FBQUNPLGlCQUFLLEVBQUUsY0FBUjtBQUF3QjdCLGdCQUFJLEVBQUUsY0FBOUI7QUFBOEM4QixpQkFBSyxFQUFFO0FBQXJELFdBQUQsRUFBc0U7QUFBQ0QsaUJBQUssRUFBRSxjQUFSO0FBQXdCN0IsZ0JBQUksRUFBRSxjQUE5QjtBQUE4QzhCLGlCQUFLLEVBQUU7QUFBckQsV0FBdEU7QUFEaUI7QUFBUCxPQUFELENBQVg7QUFHRDs7QUFDRCxRQUFJekIsR0FBRyxDQUFDbUIsUUFBSixDQUFhLGlCQUFiLENBQUosRUFBcUM7QUFDbkM7QUFDQSxhQUFPVCxJQUFJLENBQUM7QUFBQ08sWUFBSSxFQUFFO0FBQUNBLGNBQUksRUFDckIsQ0FBQztBQUFDTyxpQkFBSyxFQUFFLFFBQVI7QUFBa0I3QixnQkFBSSxFQUFFLFFBQXhCO0FBQWtDOEIsaUJBQUssRUFBRTtBQUF6QyxXQUFELEVBQW9EO0FBQUNELGlCQUFLLEVBQUUsU0FBUjtBQUFtQjdCLGdCQUFJLEVBQUUsU0FBekI7QUFBb0M4QixpQkFBSyxFQUFFO0FBQTNDLFdBQXBEO0FBRGdCO0FBQVAsT0FBRCxDQUFYO0FBR0Q7O0FBQ0RkLE9BQUcsQ0FBQyxLQUFELENBQUg7QUFDRCxHQXhDTSxDQUFQO0FBeUNELENBMUNEOztBQTJDQVosV0FBVyxDQUFDMkIsS0FBWixHQUFvQixVQUFDMUIsR0FBRCxFQUFjaUIsSUFBZCxFQUF5QmhCLE1BQXpCLEVBQXlEO0FBQUMsU0FBTyxJQUFJTyxPQUFKLENBQWlCLFVBQUNNLEdBQUQsRUFBTztBQUMzR3BCLFdBQU8sbUNBQU9BLE9BQVAsR0FBbUJ1QixJQUFuQixDQUFQLENBRDJHLENBRTNHOztBQUNBTCxXQUFPLENBQUNJLElBQVIsQ0FBYSxvQkFBYixFQUFtQ3RCLE9BQW5DO0FBQ0EsV0FBT29CLEdBQUcsQ0FBQ3BCLE9BQUQsQ0FBVjtBQUNELEdBTG9GLENBQVA7QUFLM0UsQ0FMSDs7QUFNQUssV0FBVyxVQUFYLEdBQXFCLFVBQUNDLEdBQUQsRUFBY0MsTUFBZCxFQUE4QztBQUFDLFNBQU8sSUFBSU8sT0FBSixDQUFZLFlBQUksQ0FBRSxDQUFsQixDQUFQO0FBQTJCLENBQS9GOztBQUNBVCxXQUFXLENBQUM0QixJQUFaLEdBQW1CLFVBQUMzQixHQUFELEVBQWNpQixJQUFkLEVBQXlCaEIsTUFBekIsRUFBeUQ7QUFDMUU7QUFDQSxTQUFPLElBQUlPLE9BQUosQ0FBWSxVQUFDRSxJQUFELEVBQU9DLEdBQVAsRUFBZTtBQUNoQ0MsV0FBTyxDQUFDVyxHQUFSLENBQVksTUFBWixFQUFvQnZCLEdBQXBCLEVBQXlCaUIsSUFBekI7QUFDQTs7QUFDQSxRQUFNSCxHQUFHLEdBQUcsU0FBTkEsR0FBTSxDQUFDQyxDQUFEO0FBQUEsYUFBT0wsSUFBSSxDQUFDO0FBQUNPLFlBQUksRUFBRTtBQUFDQSxjQUFJLEVBQUU7QUFBQ0MsbUJBQU8sRUFBRUg7QUFBVjtBQUFQO0FBQVAsT0FBRCxDQUFYO0FBQUEsS0FBWjs7QUFDQSxRQUFJZixHQUFHLENBQUNtQixRQUFKLENBQWEsU0FBYixDQUFKLEVBQTZCO0FBQzNCLFVBQU1TLEtBQUssR0FBR1AsUUFBUSxDQUFDckIsR0FBRyxDQUFDc0IsS0FBSixDQUFVLEtBQVYsRUFBaUIsQ0FBakIsQ0FBRCxDQUF0QjtBQUNBTCxVQUFJO0FBQ0ZHLFVBQUUsRUFBR3RCLE1BQU0sQ0FBQytCLE1BQVAsSUFBaUI7QUFEcEIsU0FFQ1osSUFGRDtBQUdGYSxrQkFBVSxFQUFFRjtBQUhWLFFBQUosQ0FGMkIsQ0FPM0I7QUFDQTs7QUFDQTlCLFlBQU0sQ0FBQ2lDLE1BQVAsQ0FBYyxDQUFkLEVBQWlCLENBQWpCLEVBQW9CZCxJQUFwQjtBQUNBLGFBQU9ILEdBQUcsQ0FBQ2hCLE1BQU0sQ0FBQ2tDLEtBQVAsQ0FBYSxDQUFiLEVBQWdCLENBQWhCLENBQUQsQ0FBVjtBQUNEOztBQUNELFFBQUloQyxHQUFHLENBQUNtQixRQUFKLENBQWEsYUFBYixDQUFKLEVBQWlDO0FBQy9CO0FBQ0FGLFVBQUk7QUFDRkcsVUFBRSxFQUFHdkIsTUFBTSxDQUFDZ0MsTUFBUCxJQUFpQixDQURwQjtBQUVGSSxlQUFPLEVBQUU7QUFDUHZDLGlCQUFPLEVBQVBBO0FBRE87QUFGUCxTQUtDdUIsSUFMRDtBQU1GaUIsNEJBQW9CLEVBQUUsSUFBSUMsSUFBSixDQUFTbEIsSUFBSSxDQUFDaUIsb0JBQWQsRUFBb0NFLFdBQXBDLEVBTnBCO0FBT0ZDLHNCQUFjLEVBQUUsSUFBSUYsSUFBSixDQUFTbEIsSUFBSSxDQUFDb0IsY0FBZCxFQUE4QkQsV0FBOUI7QUFQZCxRQUFKO0FBU0F4QixhQUFPLENBQUNXLEdBQVIsQ0FBWSxnQkFBWixFQUE4Qk4sSUFBOUIsRUFYK0IsQ0FZL0I7QUFDQTs7QUFDQXBCLFlBQU0sQ0FBQ2tDLE1BQVAsQ0FBYyxDQUFkLEVBQWlCLENBQWpCLEVBQW9CZCxJQUFwQjtBQUNBLGFBQU9ILEdBQUcsQ0FBQ2pCLE1BQU0sQ0FBQyxDQUFELENBQVAsQ0FBVjtBQUNEOztBQUNEYyxPQUFHLENBQUMsS0FBRCxDQUFIO0FBQ0QsR0FsQ00sQ0FBUDtBQW1DRCxDQXJDRDs7QUFzQ0FaLFdBQVcsQ0FBQ3VDLEdBQVosR0FBa0IsVUFBQ3RDLEdBQUQsRUFBY0MsTUFBZCxFQUE4QztBQUFDLFNBQU8sSUFBSU8sT0FBSixDQUFZLFlBQUksQ0FBRSxDQUFsQixDQUFQO0FBQTJCLENBQTVGOztBQUNBVCxXQUFXLENBQUN3QyxJQUFaLEdBQW1CLFVBQUN2QyxHQUFELEVBQWNDLE1BQWQsRUFBOEM7QUFBQyxTQUFPLElBQUlPLE9BQUosQ0FBWSxZQUFJLENBQUUsQ0FBbEIsQ0FBUDtBQUEyQixDQUE3Rjs7QUFDQVQsV0FBVyxDQUFDeUMsT0FBWixHQUFzQixVQUFDeEMsR0FBRCxFQUFjQyxNQUFkLEVBQThDO0FBQUMsU0FBTyxJQUFJTyxPQUFKLENBQVksWUFBSSxDQUFFLENBQWxCLENBQVA7QUFBMkIsQ0FBaEc7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvam9iL1tqb2JJZF0uYWFiZGY5YzU4MmQ1ODZkODYzYjcuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEF4aW9zSW5zdGFuY2UsIEF4aW9zSW50ZXJjZXB0b3JNYW5hZ2VyLCBBeGlvc1Byb21pc2UsIEF4aW9zUmVxdWVzdENvbmZpZywgQXhpb3NSZXNwb25zZSB9IGZyb20gXCJheGlvc1wiO1xyXG5cclxuLy8gZXhwb3J0IGludGVyZmFjZSBBeGlvc0luc3RhbmNlMiB7XHJcbi8vICAgKGNvbmZpZzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogQXhpb3NQcm9taXNlO1xyXG4vLyAgICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogQXhpb3NQcm9taXNlXHJcbi8vIH1cclxuXHJcbmxldCBjb21wYW55ID0ge1xyXG4gIG5hbWU6ICcnLFxyXG4gIE5JUDogJycsXHJcbn1cclxubGV0IG9yZGVycyA9IFtdIHx8IFt7aWQ6IDB9XSB8fCBbe1xyXG4gIGNyZWF0b3I6IHtcclxuICAgIGNvbXBhbnk6IHsuLi5jb21wYW55fSxcclxuICB9LFxyXG59XVxyXG5sZXQgb2ZmZXJzID0gW10gfHwgW3tcclxuICBpZDogMCxcclxuICBqb2JPZmZlcklkOiAwLFxyXG4gIHByaWNlTmV0OiAxNTAwLFxyXG59XVxyXG5cclxuZXhwb3J0IGNvbnN0IGFwaUluc3RhbmNlOiBBeGlvc0luc3RhbmNlID0gZnVuY3Rpb24gKHVybDogc3RyaW5nIHwgQXhpb3NSZXF1ZXN0Q29uZmlnLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpIHtcclxuICAvLyB0aGlzLmRlZmF1bHRzID0ge30gYXMgQXhpb3NSZXF1ZXN0Q29uZmlnO1xyXG4gIC8vIHRoaXMuaW50ZXJjZXB0b3JzID0ge1xyXG4gIC8vICAgcmVxdWVzdDoge30gYXMgQXhpb3NJbnRlcmNlcHRvck1hbmFnZXI8QXhpb3NSZXF1ZXN0Q29uZmlnPjtcclxuICAvLyAgIHJlc3BvbnNlOiB7fSBhcyBBeGlvc0ludGVyY2VwdG9yTWFuYWdlcjxBeGlvc1Jlc3BvbnNlPGFueT4+O1xyXG4gIC8vIH07XHJcbiAgXHJcbiAgLy8gdGhpcy5nZXRVcmkgPSAoKSA9PiB7cmV0dXJuICcnfTtcclxuICAvLyB0aGlzLmdldCA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHRoaXMucGF0Y2ggPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyB0aGlzLmRlbGV0ZSA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHRoaXMucG9zdCA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHRoaXMucHV0ID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gdGhpcy5yZXF1ZXN0ID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gdGhpcy5oZWFkID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gdGhpcy5vcHRpb25zID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcblxyXG4gIHJldHVybiB7fSBhcyBBeGlvc1Byb21pc2U8YW55PjtcclxufVxyXG5hcGlJbnN0YW5jZS5kZWZhdWx0cyA9IHt9IGFzIEF4aW9zUmVxdWVzdENvbmZpZ1xyXG5hcGlJbnN0YW5jZS5pbnRlcmNlcHRvcnMgPSB7XHJcbiAgcmVxdWVzdDoge30gYXMgQXhpb3NJbnRlcmNlcHRvck1hbmFnZXI8QXhpb3NSZXF1ZXN0Q29uZmlnPixcclxuICByZXNwb25zZToge30gYXMgQXhpb3NJbnRlcmNlcHRvck1hbmFnZXI8QXhpb3NSZXNwb25zZTxhbnk+PlxyXG59XHJcbmFwaUluc3RhbmNlLmdldFVyaSA9ICgpID0+IHtyZXR1cm4gJyd9O1xyXG5hcGlJbnN0YW5jZS5yZXF1ZXN0ID0gKHJlcTogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG5hcGlJbnN0YW5jZS5nZXQgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge1xyXG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzMiwgcmVqKSA9PiB7XHJcbiAgICBjb25zb2xlLndhcm4oJ0dFVCcsIHVybClcclxuICAgIC8vIGRlYnVnZ2VyXHJcbiAgICBjb25zdCByZXMgPSAocikgPT4ge1xyXG4gICAgICBjb25zb2xlLmluZm8oJ0dFVCB1cmwnLCB1cmwsIHIpXHJcbiAgICAgIHJlczIoe2RhdGE6IHtkYXRhOiB7cmVzdWx0czogcn19fSlcclxuICAgIH1cclxuICAgIGlmICh1cmwuaW5jbHVkZXMoJy9tZScpKSB7XHJcbiAgICAgIHJldHVybiByZXMyKHtkYXRhOiB7ZGF0YTogY29tcGFueX19KVxyXG4gICAgfVxyXG4gICAgaWYgKHVybC5pbmNsdWRlcygnL2pvYi1vcmRlcnMvJykgJiYgdXJsLmluY2x1ZGVzKCcvb2ZmZXJzJykpIHtcclxuICAgICAgY29uc3QgaWQgPSBwYXJzZUludCh1cmwubWF0Y2goL1xcZCsvKVswXSlcclxuICAgICAgY29uc29sZS5sb2coJy9qb2Itb3JkZXJzLzppZC9vZmZlcnMnLCBpZCwgb2ZmZXJzKVxyXG4gICAgICAvLyBkZWJ1Z2dlclxyXG4gICAgICByZXR1cm4gcmVzKG9mZmVycylcclxuICAgIH1cclxuICAgIGlmICh1cmwuaW5jbHVkZXMoJy9qb2Itb3JkZXJzLycpKSB7XHJcbiAgICAgIGNvbnN0IGlkID0gcGFyc2VJbnQodXJsLm1hdGNoKC9cXGQrLylbMF0pXHJcbiAgICAgIHJldHVybiByZXMyKHtkYXRhOiB7ZGF0YTogb3JkZXJzW2lkXX19KVxyXG4gICAgfVxyXG4gICAgaWYgKHVybC5pbmNsdWRlcygnL2pvYi1vcmRlcnMnKSkge1xyXG4gICAgICBjb25zb2xlLmxvZygnb3JkZXJzJywgb3JkZXJzKVxyXG4gICAgICByZXR1cm4gcmVzKG9yZGVycylcclxuICAgIH1cclxuICAgIC8vIGRlYnVnZ2VyXHJcbiAgICBpZiAodXJsLmluY2x1ZGVzKCcvam9iLW9mZmVycycpKSB7XHJcbiAgICAgIHJldHVybiByZXMob2ZmZXJzKVxyXG4gICAgfVxyXG4gICAgaWYgKHVybC5pbmNsdWRlcygnL2NhdGVnb3JpZXMvcHJvZCcpKSB7XHJcbiAgICAgIHJldHVybiByZXMyKHtkYXRhOiB7ZGF0YTogXHJcbiAgICAgICAgW3tsYWJlbDogJ0ZvdG93b2x0YWlrYScsIG5hbWU6ICdGb3Rvd29sdGFpa2EnLCB2YWx1ZTogJ0ZvdG93b2x0YWlrYSd9LHtsYWJlbDogJ1BvbXB5IGNpZXDFgmEnLCBuYW1lOiAnUG9tcHkgY2llcMWCYScsIHZhbHVlOiAnUG9tcHkgY2llcMWCYSd9XVxyXG4gICAgICB9fSlcclxuICAgIH1cclxuICAgIGlmICh1cmwuaW5jbHVkZXMoJy9jYXRlZ29yaWVzL2pvYicpKSB7XHJcbiAgICAgIC8vIGRlYnVnZ2VyXHJcbiAgICAgIHJldHVybiByZXMyKHtkYXRhOiB7ZGF0YTogXHJcbiAgICAgICAgKFt7bGFiZWw6ICdNb250YcW8JywgbmFtZTogJ01vbnRhxbwnLCB2YWx1ZTogJ01vbnRhxbwnfSx7bGFiZWw6ICdQcm9qZWt0JywgbmFtZTogJ1Byb2pla3QnLCB2YWx1ZTogJ1Byb2pla3QnfV0pXHJcbiAgICAgIH19KVxyXG4gICAgfVxyXG4gICAgcmVqKCdlcnInKVxyXG4gIH0pIGFzIHVua25vd24gYXMgUHJvbWlzZTxhbnk+XHJcbn07XHJcbmFwaUluc3RhbmNlLnBhdGNoID0gKHVybDogc3RyaW5nLCBkYXRhOiBhbnksIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZTxhbnk+KChyZXMpPT57XHJcbiAgY29tcGFueSA9IHsuLi5jb21wYW55LCAuLi5kYXRhfVxyXG4gIC8vIGRlYnVnZ2VyXHJcbiAgY29uc29sZS5pbmZvKCdzYXZlZCBjb21wYW5pZXMvbWUnLCBjb21wYW55KVxyXG4gIHJldHVybiByZXMoY29tcGFueSlcclxufSl9O1xyXG5hcGlJbnN0YW5jZS5kZWxldGUgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuYXBpSW5zdGFuY2UucG9zdCA9ICh1cmw6IHN0cmluZywgZGF0YTogYW55LCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtcclxuICAvLyByZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KVxyXG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzMiwgcmVqKSA9PiB7XHJcbiAgICBjb25zb2xlLmxvZygnUE9TVCcsIHVybCwgZGF0YSlcclxuICAgIGRlYnVnZ2VyXHJcbiAgICBjb25zdCByZXMgPSAocikgPT4gcmVzMih7ZGF0YToge2RhdGE6IHtyZXN1bHRzOiByfX19KVxyXG4gICAgaWYgKHVybC5pbmNsdWRlcygnL29mZmVycycpKSB7XHJcbiAgICAgIGNvbnN0IGpvYklkID0gcGFyc2VJbnQodXJsLm1hdGNoKC9cXGQrLylbMF0pXHJcbiAgICAgIGRhdGEgPSB7XHJcbiAgICAgICAgaWQ6IChvZmZlcnMubGVuZ3RoIHx8IDApLFxyXG4gICAgICAgIC4uLmRhdGEsXHJcbiAgICAgICAgam9iT3JkZXJJZDogam9iSWQsXHJcbiAgICAgIH1cclxuICAgICAgLy8gb2ZmZXJzLnB1c2goZGF0YSlcclxuICAgICAgLy8gcmV0dXJuIHJlcyhvZmZlcnMuc2xpY2UoLTEpWzBdKVxyXG4gICAgICBvZmZlcnMuc3BsaWNlKDAsIDAsIGRhdGEpXHJcbiAgICAgIHJldHVybiByZXMob2ZmZXJzLnNsaWNlKDApWzBdKVxyXG4gICAgfVxyXG4gICAgaWYgKHVybC5pbmNsdWRlcygnL2pvYi1vcmRlcnMnKSkge1xyXG4gICAgICAvLyBkZWJ1Z2dlclxyXG4gICAgICBkYXRhID0ge1xyXG4gICAgICAgIGlkOiAob3JkZXJzLmxlbmd0aCB8fCAwKSxcclxuICAgICAgICBjcmVhdG9yOiB7XHJcbiAgICAgICAgICBjb21wYW55LFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgLi4uZGF0YSxcclxuICAgICAgICBhY2NlcHRpbmdPZmZlcnNVbnRpbDogbmV3IERhdGUoZGF0YS5hY2NlcHRpbmdPZmZlcnNVbnRpbCkudG9JU09TdHJpbmcoKSxcclxuICAgICAgICBleGVjdXRpb25VbnRpbDogbmV3IERhdGUoZGF0YS5leGVjdXRpb25VbnRpbCkudG9JU09TdHJpbmcoKSxcclxuICAgICAgfVxyXG4gICAgICBjb25zb2xlLmxvZygnUE9TVCBqb2Itb3JkZXInLCBkYXRhKVxyXG4gICAgICAvLyBvcmRlcnMucHVzaChkYXRhKVxyXG4gICAgICAvLyByZXR1cm4gcmVzKG9yZGVycy5zbGljZSgtMSlbMF0pXHJcbiAgICAgIG9yZGVycy5zcGxpY2UoMCwgMCwgZGF0YSlcclxuICAgICAgcmV0dXJuIHJlcyhvcmRlcnNbMF0pXHJcbiAgICB9XHJcbiAgICByZWooJ2VycicpXHJcbiAgfSkgYXMgdW5rbm93biBhcyBQcm9taXNlPGFueT5cclxufTtcclxuYXBpSW5zdGFuY2UucHV0ID0gKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpID0+IHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbmFwaUluc3RhbmNlLmhlYWQgPSAodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykgPT4ge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuYXBpSW5zdGFuY2Uub3B0aW9ucyA9ICh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSA9PiB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG5cclxuLypcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgQVBJMiBpbXBsZW1lbnRzIEF4aW9zSW5zdGFuY2Uge1xyXG4gIC8vIGNvbnN0cnVjdG9yKHVybDogc3RyaW5nIHwgQXhpb3NSZXF1ZXN0Q29uZmlnLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpIHtcclxuICAvLyB9XHJcbiAgY29uc3RydWN0b3IoY29uZmlnOiBBeGlvc1JlcXVlc3RDb25maWcpOiBBeGlvc1Byb21pc2U8YW55PiB7XHJcbiAgfVxyXG4gIGRlZmF1bHRzOiB7fTtcclxuICAvLyBpbnRlcmNlcHRvcnM6IHtyZXF1ZXN0OiB7dXNlKCl7cmV0dXJuIDB9LCBlamVjdCgpe319LCByZXNwb25zZToge3VzZSgpe3JldHVybiAwfSwgZWplY3QoKXt9fX07XHJcbiAgaW50ZXJjZXB0b3JzOiB7XHJcbiAgICByZXF1ZXN0OiBBeGlvc0ludGVyY2VwdG9yTWFuYWdlcjxBeGlvc1JlcXVlc3RDb25maWc+O1xyXG4gICAgcmVzcG9uc2U6IEF4aW9zSW50ZXJjZXB0b3JNYW5hZ2VyPEF4aW9zUmVzcG9uc2U8YW55Pj47XHJcbiAgfTtcclxuICAvLyBnZXRVcmkoKXtyZXR1cm4gJyd9O1xyXG4gIC8vIGdldCh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHBhdGNoKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpIHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gZGVsZXRlKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpIHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gcG9zdCh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHB1dCh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIC8vIHJlcXVlc3QodXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZykge3JldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pfTtcclxuICAvLyBoZWFkKHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpIHtyZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KX07XHJcbiAgLy8gb3B0aW9ucyh1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKSB7cmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSl9O1xyXG4gIGdldFVyaShjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpOiBzdHJpbmcge3JldHVybiAnJ307XHJcbiAgcmVxdWVzdDxUID0gYW55LCBSID0gQXhpb3NSZXNwb25zZTxUPj4gKGNvbmZpZzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogUHJvbWlzZTxSPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KVxyXG4gIH07XHJcbiAgZ2V0PFQgPSBhbnksIFIgPSBBeGlvc1Jlc3BvbnNlPFQ+Pih1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogUHJvbWlzZTxSPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KVxyXG4gIH07XHJcbiAgZGVsZXRlPFQgPSBhbnksIFIgPSBBeGlvc1Jlc3BvbnNlPFQ+Pih1cmw6IHN0cmluZywgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogUHJvbWlzZTxSPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KVxyXG4gIH07XHJcbiAgaGVhZDxUID0gYW55LCBSID0gQXhpb3NSZXNwb25zZTxUPj4odXJsOiBzdHJpbmcsIGNvbmZpZz86IEF4aW9zUmVxdWVzdENvbmZpZyk6IFByb21pc2U8Uj4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKCgpPT57fSlcclxuICB9O1xyXG4gIG9wdGlvbnM8VCA9IGFueSwgUiA9IEF4aW9zUmVzcG9uc2U8VD4+KHVybDogc3RyaW5nLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpOiBQcm9taXNlPFI+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pXHJcbiAgfTtcclxuICBwb3N0PFQgPSBhbnksIFIgPSBBeGlvc1Jlc3BvbnNlPFQ+Pih1cmw6IHN0cmluZywgZGF0YT86IGFueSwgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogUHJvbWlzZTxSPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KVxyXG4gIH07XHJcbiAgcHV0PFQgPSBhbnksIFIgPSBBeGlvc1Jlc3BvbnNlPFQ+Pih1cmw6IHN0cmluZywgZGF0YT86IGFueSwgY29uZmlnPzogQXhpb3NSZXF1ZXN0Q29uZmlnKTogUHJvbWlzZTxSPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKCk9Pnt9KVxyXG4gIH07XHJcbiAgcGF0Y2g8VCA9IGFueSwgUiA9IEF4aW9zUmVzcG9uc2U8VD4+KHVybDogc3RyaW5nLCBkYXRhPzogYW55LCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpOiBQcm9taXNlPFI+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgoKT0+e30pXHJcbiAgfTtcclxufVxyXG5cclxuLy8gZXhwb3J0IGNvbnN0IGFwaUluc3RhbmNlOiBBeGlvc0luc3RhbmNlID0gZnVuY3Rpb24gKHVybDogc3RyaW5nIHwgQXhpb3NSZXF1ZXN0Q29uZmlnLCBjb25maWc/OiBBeGlvc1JlcXVlc3RDb25maWcpOiBBeGlvc1Byb21pc2Uge1xyXG4vLyB9XHJcblxyXG4qLyJdLCJzb3VyY2VSb290IjoiIn0=